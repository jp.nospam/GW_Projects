#cs ----------------------------------------------------------------------------
    Author:         jp.nospam
    Version:        0.1

    Script Function:
        A Guild Wars 2 helper/Macro

    To-Do:
        1.  Fix hotkeyset

    Change Log:
        0.1
            Updated timers to not show if timer is set to 0.
#ce ----------------------------------------------------------------------------

; Custom Includes
#include <Misc.au3>
#include <Crypt.au3>
#include <WinAPI.au3>

; Koda Includes
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <WindowsConstants.au3>

; Set Options
AutoItSetOption ("TrayAutoPause", 0)
AutoItSetOption ("PixelCoordMode", 2)
AutoItSetOption ("MouseCoordMode", 2)
AutoItSetOption("SendCapslockMode", 0)
AutoItSetOption("SendKeyDownDelay", 0)
AutoItSetOption("SendKeyDelay", 0)

; Global Constants
global const $sFile = "gw2helper.ini"
global const $proc = getSetting("processName", "Gw2.exe")
global const $winName = getSetting("windowName", "Guild Wars 2")
global const $hAlg = $CALG_AES_256
global const $exePath = getSetting("path", "C:\Program Files (x86)\Guild Wars 2\Gw2.exe")

; Form creation
$mainForm = GUICreate("GW2 Helper", 114, 151, 192, 114)
$accountList = GUICtrlCreateList("", 0, 0, 113, 149,0)
$listMenu = GUICtrlCreateContextMenu($accountList)
$loginID = GUICtrlCreateMenuItem("Login", $listMenu)
$keyID = GUICtrlCreateMenuItem("Key", $listMenu)
GUICtrlCreateMenuItem("", $listMenu, 2)
$addID = GUICtrlCreateMenuItem("Add", $listMenu)
$editID = GUICtrlCreateMenuItem("Edit", $listMenu)
$deleteID = GUICtrlCreateMenuItem("Delete", $listMenu)

; Global Variables
global $tog = 0
global $eKey = NULL
global $accounts = NULL

; Only allow 1 gw2helper exe
_Singleton("gw2helper")

; Set hotkeys
hotkeyset(getSetting("chatmacro","{F2}"), "chatMacro")
hotkeyset(getSetting("clicker","{F3}"), "clicker")
hotkeyset(getSetting("login","{F9}"), "createLogin")

#cs
    Function:       createLogin

    Parameters:
        None

    Return:
        None

    Description:    Show the login GUI
#ce
func createLogin()
    GUISetState(@SW_SHOW, $mainForm)
    if $eKey = NULL then $eKey = inputbox ("Key?", "Input decryption key.")
    updateList()
endfunc

#cs
    Function:       getSetting

    Parameters:
        $key        The key to get from the settings section of the Ini.

        $default    The default value to return if it is not found.

    Return:
        None

    Description:    Get a setting from the gw2helper ini file.
#ce
func getSetting($key,$default)
    return iniread($sFile, "settings", $key, $default)
endfunc

#cs
    Function:       encryptAccount

    Parameters:
        $user       The username to encrypt.

        $pass       The Password to encrypt.

    Return:
        Array[0]    Encrypted username.

        Array[1]    Encrypted password.

    Description:    Function to encrypt username and password. Returns an array with encrypted data.
#ce
func encryptAccount($user,$pass)
    _Crypt_Startup()

    local $info[2]

    $info[0] = _Crypt_EncryptData($user, $eKey, $hAlg)
    $info[0] = "0x" & Hex($info[0])

    $info[1] = _Crypt_EncryptData($pass, $eKey, $hAlg)
    $info[1] = "0x" & Hex($info[1])

    _Crypt_Shutdown()

    return $info
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func decryptAccount($user, $pass)
    _Crypt_Startup()

    local $info[2]

    $info[0] = _Crypt_DecryptData(Binary($user), $eKey, $hAlg)
    $info[0] = BinaryToString($info[0])

    $info[1] = _Crypt_DecryptData(Binary($pass), $eKey, $hAlg)
    $info[1] = BinaryToSTring($info[1])

    _Crypt_Shutdown()

    return $info
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func updateList()
    GUICtrlSetData($accountList, "")
    $accounts = inireadsection($sFile, "accounts")
    if IsArray($accounts) then
        for $i = 1 to $accounts[0][0]
            _GUICtrlListBox_AddString($accountList, $accounts[$i][0])
        next
    endif
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func loginAccount()
    $id = _GUICtrlListBox_GetText($accountList, _GUICtrlListBox_GetCurSel($accountList))

    $user = iniread($sFile, "accounts", $id, "")
    $pass = iniread($sFile, "passwords", $id, "")

    if $user <> "" and $pass <> "" Then
        $info = decryptAccount($user, $pass)
        ProcessClose($proc)
        ProcessWaitClose($proc)
        run($exePath & " -email " & $info[0] & " -password " & $info[1] & " -nopatchui")
        GuiSetState(@SW_HIDE, $mainForm)
    endif
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func addAccount()
$id = inputbox("ID?", "Give an ID")
$user = inputbox("Username?", "Input Username.")
$pass = inputbox("Password?", "Input password.")

if $id == "" or $user == "" or $pass == "" then return

$info = encryptAccount($user, $pass)

iniwrite($sFile, "accounts", $id, $info[0])
iniwrite($sFile, "passwords", $id, $info[1])

updateList()
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func editAccount()
$id = _GUICtrlListBox_GetText($accountList, _GUICtrlListBox_GetCurSel($accountList))
$user = inputbox("Usernamame?", "Input Username.")
$pass = inputbox("Password?", "Input password.")

if $id == "" or $user == "" or $pass == "" then return

$info = encryptAccount($user, $pass)
iniwrite($sFile, "accounts", $id, $info[0])
iniwrite($sFile, "passwords", $id, $info[1])

updateList()
EndFunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func deleteAccount()
$id = _GUICtrlListBox_GetText($accountList, _GUICtrlListBox_GetCurSel($accountList))

IniDelete($sFile, "accounts", $id)
iniDelete($sFile, "passwords", $id)

updateList()
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func chatMacro()
    if winactive($winName) then
        send("{enter}{up}{enter}")
    endif
endfunc

#cs
    Function:

    Parameters:


    Return:


    Description:
#ce
func clicker()
    if $tog == 0  then
        $tog = 1
    Else
        $tog = 0
    endif
endfunc

while 1
    while $tog == 1
        if winactive($winName) then
            mouseclick("left")
        endif
    wend

    $nMsg = GUIGetMsg()
	Switch $nMsg
    Case $GUI_EVENT_MINIMIZE
        GuiSetState(@SW_HIDE, $mainForm)
    Case $GUI_EVENT_CLOSE
        GuiSetState(@SW_HIDE, $mainForm)
        if not ProcessExists($proc) then Run($exePath)
    Case $loginID
        loginAccount()
    Case $keyID
        $eKey = inputbox ("Key?", "Input decryption key.")
    Case $addID
        addAccount()
    Case $editID
        editAccount()
    Case $deleteID
        deleteAccount()
	EndSwitch
wend