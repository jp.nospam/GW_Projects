#NoTrayIcon

AutoItSetOption("SendCapslockMode", 0)
AutoItSetOption("SendKeyDownDelay", 0)
AutoItSetOption("SendKeyDelay", 0)

#include <File.au3>
#include <GuiComboBox.au3>
#include <Misc.au3>

#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

#Region ### START Koda GUI section ### Form=
$Form1 = GUICreate("GW2 DJ", 497, 435, 192, 114)
$exportEdit = GUICtrlCreateEdit("", 8, 48, 260, 353, BitOR($ES_AUTOVSCROLL,$ES_WANTRETURN,$WS_VSCROLL))
GUICtrlSetLimit(-1, 999999999)
GUICtrlSetData(-1, "")
$exportButton = GUICtrlCreateButton("Export", 184, 8, 75, 25)
$playButton = GUICtrlCreateButton("Play", 296, 8, 75, 25)
$titleInput = GUICtrlCreateInput("", 40, 8, 121, 21)
$songList = GUICtrlCreateList("", 288, 80, 193, 305)
GUICtrlSetData(-1, "")
$Label1 = GUICtrlCreateLabel("Title:", 8, 8, 30, 17)
$instrumentCombo = GUICtrlCreateCombo("", 296, 48, 177, 25, BitOR($GUI_SS_DEFAULT_COMBO,$CBS_SIMPLE))
GUICtrlSetData(-1, "")
$announceCheckbox = GUICtrlCreateCheckbox("Announce", 296, 392, 73, 17)
$djCheckbox = GUICtrlCreateCheckbox("DJ Mode", 384, 392, 65, 17)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

global $instruments

readSettings()
updateList()


func convertahk($name)
    
    if $name = "" then
        MsgBox(0, "Invalid Name", "Name cannot be empty!")
        return
    endif

    $filter = getExt()

    $file = fileOpen("library/"& $name & $filter, 2)
    $text = guictrlread($exportEdit)
    
    $newtext = StringRegExpReplace($text, "Sleep, ([0-9]+)", "$1")
    $newtext = StringRegExpReplace($newtext, "SendInput {([a-zA-Z0-9 ]+)}", "$1")
    
    filewrite($file, $newtext)
    FileClose($file)
    
    guictrlsetdata($exportEdit, "")
    guictrlsetdata($titleInput, "")

endfunc

func runScript($songName)
    $ext = getExt()
        
    $filename = "./library/" & $songName & $ext
    
    if not fileexists($filename) then
        msgbox(0, "File Error", "Unable to open file!")
        return
    endif
    
    $file = fileopen($filename, 0)
    
    winwaitactive("Guild Wars 2")
    
    if guictrlread($announceCheckbox) == $GUI_CHECKED Then
        send("{Enter}")
        sleep(500)
        Send("Now playing: " & $songName)
        send("{Enter}")
        sleep(500)
    endif
    send("{Numpad0}")
    sleep(100)
    send("{Numpad0}")
    sleep(100)
    send("{Numpad9}")
    sleep(100)
    While 1
        Local $line = FileReadLine($file)
        If @error = -1 or _IsPressed(76) Then ExitLoop
        if stringisdigit($line) then
            sleep($line)
        else
            send("{"& $line &"}")
        endif
    WEnd
    
    fileclose($file)
endfunc

func djMode()
    $songNum = _GUICtrlListBox_GetCount($songList)
    if $songNum < 1 Then
        msgbox(0, "No songs!", "No songs to DJ!")
        return
    endif
    
    WinWaitActive("Guild Wars 2")
    for $i = 0 to $songNum - 1
        If _IsPressed(77) or not winactive("Guild Wars 2") Then ExitLoop
        runScript(_GUICtrlListBox_GetText($songList, $i))
    Next
endfunc

func readSettings()
    $instruments = inireadSection("gw2dj.ini", "instruments")
    
    for $i = 1 to $instruments[0][0]
        _GUICtrlComboBox_AddString($instrumentCombo, $instruments[$i][0])
    next
    
    _GUICtrlComboBox_SetCurSel($instrumentCombo, 0)
endfunc

func getExt()

    $inst = guictrlread($instrumentCombo)
    
    for $i = 1 to $instruments[0][0]
        if $inst == $instruments[$i][0] then return $instruments[$i][1]
    next
        
endfunc

func updateList()
    $filter = getExt()

    $list = _FileListToArray("./library", "*" & $filter, 1)
    guictrlsetdata($songList, "")
    if $list <> 0 then
        for $i = 1 to $list[0]
            $name = stringsplit($list[$i], ".")
            _GUICtrlListBox_AddString($songList, $name[1])
        next
    endif
endfunc


While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
        case $playButton
            if guictrlread($djCheckbox) == $GUI_CHECKED Then
                djMode()
            else
                runScript(guictrlread($songList))
            endif
        case $exportButton
            convertAHK(guictrlread($titleInput))
            updateList()
        case $instrumentCombo
            updateList()

	EndSwitch
WEnd
