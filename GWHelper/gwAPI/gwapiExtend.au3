#include-once
#include "gwAPI.au3"
#include "gwhGlobals.au3"

#cs
    Function:       ext_findBossPtr

    Parameters:
        $modID      Model ID of the boss. If no ID is specified, it will
					just return the first boss it finds.

    Return:
		Pointer		Boss agent pointer.

        -1			If no boss was found.
#ce
FUNC ext_findBossPtr($modID = -1)
	FOR $i = 1 TO GetMaxAgents()
		$fAgent = GetAgentPtr($i)
		IF GetIsBoss($fAgent) AND GetIsLiving($fAgent) THEN
			IF $modID == -1 THEN RETURN $fAgent
			IF $modID == GetMemoryValue($fAgent, $mAgentPlayerNumber) THEN RETURN $fAgent
		ENDIF
	NEXT
	RETURN -1
ENDFUNC



#cs
    Function:       ext_GetIsCasting

    Parameters:
        $aAgent		Agent or Pointer ot check

    Return:
		True		Agent is using a skill.

		False		Agent is not using a skill.
#ce
Func ext_GetIsCasting($aAgent)
	local $cSkill

	If IsPtr($aAgent) <> 0 Then
		$cSkill = MemoryRead($aAgent + 436, 'word')
		If $cSkill == 2340 THEN return false
		Return $cSkill <> 0
   ElseIf IsDllStruct($aAgent) <> 0 Then
	   $cSkill = DllStructGetData($aAgent, 'Skill')
		If $cSkill == 2340 THEN return false
	   Return $cSkill <> 0
   Else
	   $cSkill = MemoryRead(GetAgentPtr($aAgent) + 436, 'word')
		If $cSkill == 2340 THEN return false
	  Return $cSkill <> 0
   EndIf
EndFunc

#cs
    Function:       	ext_getNearestItemPtrToPlayer

    Parameters:
        $aCanPickup		TRUE/FALSE. Whether or not to only find items that
						you can pick up.

    Return:
		Pointer			If item was found to pickup.

		-1				If no items are available for pick up.
#ce
FUNC ext_GetNearestItemPtrToPlayer($aCanPickUp = True)
	Local $lAgentX, $lAgentY, $lArrayX, $lArrayY
	Local $lAgentID = MemoryRead($selfPtr + 44, 'long')

	Local $lNearestAgent = -1, $lDistance, $lNearestDistance = 100000000
	Local $lAgentArray = GetAgentPtrArrayEx(1, 0x400)
	For $i = 1 To $lAgentArray[0]
		If $aCanPickUp And Not GetCanPickUp($lAgentArray[$i]) Then ContinueLoop
		If MemoryRead($lAgentArray[$i] + 44, 'long') = $lAgentID Then ContinueLoop
		IF GetMemoryValue($lAgentArray[$i], $mAgentType) == 6 THEN CONTINUELOOP
		$lDistance = GetDistance($lAgentArray[$i], $selfPtr)
		If $lDistance < $lNearestDistance Then
			$lNearestAgent = $lAgentArray[$i]
			$lNearestDistance = $lDistance
		EndIf
	Next

	SetExtended(Sqrt($lNearestDistance))
	Return $lNearestAgent
ENDFUNC

#cs
    Function:       	ext_MoveTo

    Parameters:
        $aX				X location to reach.

		$aY				Y location to reach.

		$aRandom		Randomizer.

    Return:
		0				Made it to destination.

		1				Player no longer exists.

		2				Died.
#ce
Func ext_MoveTo($aX, $aY, $aRandom = 50)
	Local $lBlocked = 0
	Local $lDestX = $aX + Random(-$aRandom, $aRandom)
	Local $lDestY = $aY + Random(-$aRandom, $aRandom)

	Move($lDestX, $lDestY, 0)

	Do
		Sleep(100)
		IF NOT GetAgentExists($selfPtr) Then Return 1

		If GetMemoryValue($selfPtr, $mAgentHP) <= 0 Then return 2

		If GetMemoryValue($selfPtr, $mAgentMoveX) == 0 And GetMemoryValue($selfPtr, $mAgentMoveY) == 0 Then
			$lBlocked += 1
			$lDestX = $aX + Random(-$aRandom, $aRandom)
			$lDestY = $aY + Random(-$aRandom, $aRandom)
			Move($lDestX, $lDestY, 0)
		EndIf
	Until ComputeDistance(GetMemoryValue($selfPtr, $mAgentX), GetMemoryValue($selfPtr, $mAgentY), $lDestX, $lDestY) < 25 Or $lBlocked > 14

	return 0
EndFunc

#cs
    Function:       	ext_MoveTilRange

    Parameters:
        $aX				X location to reach.

		$aY				Y location to reach.

		$aRandom		Randomizer.

    Return:
		-1				Player no longer exists.

		0				Made it to destination.

		1				Within range of an enemy.

		2				Died on the way to destination.

	Description:		Moves until within range of an enemy.
#ce
Func ext_MoveTilRange($aX, $aY, $aggroRange = 1250, $aRandom = 50)
	IF GetDistance(GetNearestEnemyPtrToAgent()) <= $aggroRange Then RETURN 1

	Local $lBlocked = 0
	Local $lDestX = $aX + Random(-$aRandom, $aRandom)
	Local $lDestY = $aY + Random(-$aRandom, $aRandom)

	IF NOT GetAgentExists($selfPtr) Then RETURN -1
	Move($lDestX, $lDestY, 0)

	Do
		Sleep(100)
		IF NOT GetAgentExists($selfPtr) Then RETURN -1
		IF GetDistance(GetNearestEnemyPtrToAgent()) <= $aggroRange Then RETURN 1

		IF GetMemoryValue($selfPtr, $mAgentHP) <= 0 Then RETURN 2

		If GetMemoryValue($selfPtr, $mAgentMoveX) == 0 And GetMemoryValue($selfPtr, $mAgentMoveY) == 0 Then
			$lBlocked += 1
			$lDestX = $aX + Random(-$aRandom, $aRandom)
			$lDestY = $aY + Random(-$aRandom, $aRandom)
			Move($lDestX, $lDestY, 0)
		EndIf

	Until ComputeDistance(GetMemoryValue($selfPtr, $mAgentX), GetMemoryValue($selfPtr, $mAgentY), $lDestX, $lDestY) < 25 Or $lBlocked > 14

	Return 0
EndFunc


#cs
    Function:       ext_identifyBag

    Parameters:
        $aBag		Bag id or ptr to start identifying.

		$aWhites	TRUE/FALSE. Whether or not to identify white items.

		$vLimit		The minimum vendor sell price to identify whites.

		$aGolds		TRUE/FALSE. Whether or not to identify gold items.

    Return:
		None
#ce
Func ext_IdentifyBag($aBag, $aWhites = false, $vLimit = 0, $aGolds = True)
	Local $lItem
	If Not IsPtr($aBag) Then $aBag = GetBagPtr($aBag)
	For $i = 1 To GetMemoryValue($aBag, $mBagSlots)
		$lItem = GetItemPtrBySlot($aBag, $i)
		If GetMemoryValue($lItem, $mItemId) == 0 Then ContinueLoop
		If GetRarity($lItem) == 2621 And $aWhites == False Then ContinueLoop
		If GetRarity($lItem) == 2624 And $aGolds == False Then ContinueLoop
        if $vLimit > 0 Then
            $iValue = GetMemoryValue($lItem, $mItemValue)
            if $iValue < $vLimit and GetRarity($lItem) == 2621 then ContinueLoop
        endif
		IdentifyItem($lItem)
		Sleep(GetPing())
	Next
EndFunc

#cs
    Function:       	ext_getBagSpace

    Parameters:
		None

    Return:
        Int				Amount of bag slots free.

    Description:   		Returns then amount of bag space.
#ce
FUNC ext_getBagSpace()
	$temp = 0

	For $b = 1 To 4
		$bag = GetBagPtr($b)
		$temp += (GetMemoryValue($bag, $mBagSlots) - GetMemoryValue($bag, $mBagItemsCount))
	next

	Return $temp
ENDFUNC

#cs
    Function:       	botLooter

    Parameters:
        $moveOOR		Move to out of range loot

		$ALD		   	Max radius it will search for dropped items.

        $EIR		    Radius to search for enemies around the item.
                        Will NOT loot if an enemy is within radius to item.

        $EPR			Radius to search for enemies around the player.
                        Will NOT loot if an enemy is within radius to the player.


    Return:
        None

    Description:    Wrapping function for autolooting on bots.
#ce
FUNC botLooter($moveOOR = FALSE, $ALD = -1, $EIR = -1, $EPR = -1)
	IF $ALD < 0 THEN $ALD = $autolootdistance
	if $EIR < 0 THEN $EIR = $enemyItemRadius
	if $EPR < 0 THEN $EPR = $enemyPlayerRadius

	$lootStatus = lootItems($ALD, $EIR, $EPR)
	WHILE $lootStatus <> 1
		if $lootStatus == 2 AND $moveOOR == FALSE theN Return
		if $lootstatus == 2 AND $moveOOR == TRUE Then
			$farItem = ext_GetNearestItemPtrToPlayer()
			if $farItem == -1 then return
			Move(GetMemoryValue($farItem, $mAgentX), GetMemoryValue($farItem, $mAgentY))
		ENDIF
		$lootStatus = lootItems($ALD, $EIR, $EPR)
		sleep(100)
	WEND
ENDFUNC

#cs
    Function:       	ext_TravelTo()

    Parameters:
		$mapID			Map ID to travel to.


    Return:
        None

    Description:    Used like regular TravelTo, except uses my map waiting function.
#ce
FUNC ext_TravelTo($mapID)
	ZoneMap($mapID)
	WaitForMap()
ENDFUNC

#cs
    Function:       	BagCleaner()

    Parameters:
		$merchX			Coordinate X near a merchant.

		$merchY			Coordinate Y near a merchant.

		$maxBagSpace	Maximum bag space allowed before it cleans.

    Return:
        0				Bags properly cleaned.

		1				Unable to clean the bags properly

    Description:    Moves to nearest NPC (needs to be merchant) specified coords and uses auto-id,
					auto-salvage, and auto-sell (respectively) to clean bags when $maxBagSpace is met.
#ce
Func BagCleaner($merchX, $merchY, $maxBagSpace)
		logGWH("Cleaning bags.")
		IF ext_GetBagSpace() < $maxBagSpace Then
			GoToNPC(GetNearestNPCPtrToCoords($merchX, $merchY))
			if idAll($idWhite, $minIDVal) == 0 Then
				BuyItem(5, 1, 100)
				idAll($idWhite, $minIDVal)
			ENDIF
			salvageAll($iniSalvagables)
			sellAll()
		ENDIF

		IF ext_GetBagSpace() < $maxBagSpace Then
			logGWH("Unable to clean bags."&@CRLF&"Exiting.")
			Return 1
		ENDIF
		logGWH("Bags cleaned.")
		Return 0
ENDFUNC

; Functions same as gwAPI, just a fix applied.
Func ext_LoadSkillTemplate($aTemplate, $aHeroNumber = 0)
   Local $lHeroID = GetHeroID($aHeroNumber)
   Local $lSplitTemplate = StringSplit($aTemplate, "")
   Local $lAttributeStr = ""
   Local $lAttributeLevelStr = ""
   Local $lTemplateType ; 4 Bits
   Local $lVersionNumber ; 4 Bits
   Local $lProfBits ; 2 Bits -> P
   Local $lProfPrimary ; P Bits
   Local $lProfSecondary ; P Bits
   Local $lAttributesCount ; 4 Bits
   Local $lAttributesBits ; 4 Bits -> A
   ;Local $lAttributes[1][2] ; A Bits + 4 Bits (for each Attribute)
   Local $lSkillsBits ; 4 Bits -> S
   Local $lSkills[8] ; S Bits * 8
   Local $lOpTail ; 1 Bit
   $aTemplate = ""
   For $i = 1 To $lSplitTemplate[0]
	  $aTemplate &= Base64ToBin64($lSplitTemplate[$i])
   Next
   $lTemplateType = Bin64ToDec(StringLeft($aTemplate, 4))
   $aTemplate = StringTrimLeft($aTemplate, 4)
   If $lTemplateType <> 14 Then Return False
   $lVersionNumber = Bin64ToDec(StringLeft($aTemplate, 4))
   $aTemplate = StringTrimLeft($aTemplate, 4)
   $lProfBits = Bin64ToDec(StringLeft($aTemplate, 2)) * 2 + 4
   $aTemplate = StringTrimLeft($aTemplate, 2)
   $lProfPrimary = Bin64ToDec(StringLeft($aTemplate, $lProfBits))
   $aTemplate = StringTrimLeft($aTemplate, $lProfBits)
   If $lProfPrimary <> GetHeroProfession($aHeroNumber) Then Return False
   $lProfSecondary = Bin64ToDec(StringLeft($aTemplate, $lProfBits))
   $aTemplate = StringTrimLeft($aTemplate, $lProfBits)
   $lAttributesCount = Bin64ToDec(StringLeft($aTemplate, 4))
   $aTemplate = StringTrimLeft($aTemplate, 4)
   $lAttributesBits = Bin64ToDec(StringLeft($aTemplate, 4)) + 4
   $aTemplate = StringTrimLeft($aTemplate, 4)
   For $i = 1 To $lAttributesCount
	  ;Attribute ID
	  $lAttributeStr &= Bin64ToDec(StringLeft($aTemplate, $lAttributesBits))
	  If $i <> $lAttributesCount Then $lAttributeStr &= "|"
	  $aTemplate = StringTrimLeft($aTemplate, $lAttributesBits)
	  ;Attribute level of above ID
	  $lAttributeLevelStr &= Bin64ToDec(StringLeft($aTemplate, 4))
	  If $i <> $lAttributesCount Then $lAttributeLevelStr &= "|"
	  $aTemplate = StringTrimLeft($aTemplate, 4)
   Next
   $lSkillsBits = Bin64ToDec(StringLeft($aTemplate, 4)) + 8
   $aTemplate = StringTrimLeft($aTemplate, 4)
   For $i = 0 To 7
	  $lSkills[$i] = Bin64ToDec(StringLeft($aTemplate, $lSkillsBits))
	  $aTemplate = StringTrimLeft($aTemplate, $lSkillsBits)
   Next
   $lOpTail = Bin64ToDec($aTemplate)
   ChangeSecondProfession($lProfSecondary, $aHeroNumber)
   SetAttributes($lAttributeStr, $lAttributeLevelStr, $aHeroNumber)
   LoadSkillBar($lSkills[0], $lSkills[1], $lSkills[2], $lSkills[3], $lSkills[4], $lSkills[5], $lSkills[6], $lSkills[7], $aHeroNumber)
EndFunc   ;==>LoadSkillTemplate