_ArrayAdd($loadedBotFuncs, "glintBot")

FUNC glintBot()
	LOCAL $sBarPtr, $chestPtr, $stayX = -4012, $stayY = 104, $maxBagSpace = 20

	While 1
		IF GetMapID() <> 652 THEN ext_TravelTo(652)

		ext_MoveTo(589, 1508)

		if BagCleaner(-1763, 695, $maxBagSpace) == 1 THEN EXIT

		ext_MoveTo(589, 1508)
		GoToNPC(GetNearestNPCPtrToCoords(2421, 3652))
		dialog(0x86)
		WaitForMap()

		$sBarPtr = GetSkillBarPtr()

		; Move to cliff
		CommandAll($stayX, $stayY)
		ext_MoveTo($stayX, $stayY, 0)

		Do
			; Fight
			$fResult = glintFight($sBarPtr)

			; Move back to cliff if skill moved you
			if $fResult == 0 AND GetMemoryValue($selfPtr, $mAgentX) <> $stayX AND GetMemoryValue($selfPtr, $mAgentY) <> $stayY THEN ext_MoveTo(-4012, 104, 0)

			; Have your heroes go kill last remaining mob.
			IF GetNumberOfFoesInRangeOfAgent($selfPtr) == 1 Then
				$enemyPtr = GetNearestEnemyPtrToAgent()
				If NOT GetIsBoss($enemyPtr) AND GetDistance($selfPtr, $enemyPtr) > 1250 THEN
					CommandAll(GetMemoryValue($enemyPtr, $mAgentX), GetMemoryValue($enemyPtr, $mAgentY))
					Do
						Sleep(100)
					Until GetIsDead($enemyPtr) Or GetNumberOfFoesInRangeOfAgent($selfPtr) > 1
					CommandAll($stayX, $stayY)
				ENDIF
			ENDIF

			; Exit loop if chest has spawned
			$chestPtr = GetNearestSignpostPtrToCoords(-3207, 1011)
			If IsPtr($chestPtr) Then ExitLoop
			sleep(100)
		Until GetMapLoading() = 2

		; Get loot
		If GetMapLoading() <> 2 Then
			GoToSignpost($chestPtr)
			botLooter(TRUE, -1, 0, 0)
		ENDIF
	WEnd

ENDFUNC


FUNC glintFight($skillBarPointer)
		$enemyPtr = GetNearestEnemyPtrToAgent()

		If NOT IsPtr($enemyPtr) Then Return 1

		$eDist = GetDistance($enemyPtr, $selfPtr)
		if $eDist > 1250 Then Return 2

		For $s = 1 TO 7
			If GetSkillBarSkillId($s) > 0 AND GetSkillbarSkillRecharge($s, 0, $skillBarPointer) == 0 AND NOT GetIsCasting($selfPtr) Then
				ChangeTarget($enemyPtr)
				UseSkill($s,$enemyPtr)
				Sleep(150)
				Return 0
			EndIf
		Next

ENDFUNC