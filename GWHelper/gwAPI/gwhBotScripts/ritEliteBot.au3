_ArrayAdd($loadedBotFuncs,"ritEliteBot")

FUNC ritEliteBot()
	IF GetMapID() <> 51 THEN TravelTo(51)

	local $bossPtr = 0
	SwitchMode(1)

	While 1
		IF GetMapID() <> 51 THEN TravelTo(51)

		ext_MoveTo(8106, -13937, 0)
        Move(6247, -12997, 0)
		waitForMap()

		CommandAll(8343, -9806)
		ext_MoveTo(8343, -9806, 0)
		CommandAll(8346, -7107)
		ext_MoveTo(8346, -7107, 0)
		$bossPtr = ext_findBossPtr()

		DO
			Sleep(100)
		UNTIL GetDistance($bossPtr) < 1250

		CancelAll()
		Attack($bossPtr)

		$curX = GetMemoryValue($selfPtr, $mAgentX)
		$curY = GetMemoryValue($selfPtr, $mAgentY)

		DO
			IF  ComputeDistance($curX, $curY, GetMemoryValue($selfPtr, $mAgentX), GetMemoryValue($selfPtr, $mAgentY)) > 2500 THEN CONTINUELOOP (2)
			sleep(100)
		UNTIL GetIsDead($bossPtr) = TRUE

		botLooter(TRUE, -1, 0, 0)
	WEND
ENDFUNC