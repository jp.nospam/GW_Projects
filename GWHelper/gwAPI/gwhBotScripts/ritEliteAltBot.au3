_ArrayAdd($loadedBotFuncs,"ritEliteAltBot")

FUNC ritEliteAltBot()
	IF GetMapID() <> 298 THEN TravelTo(298)

	local $bossPtr = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 298 THEN TravelTo(298)

		ext_MoveTo(-8559, -317, 0)
		ext_MoveTo(-9399, -1754, 0)
        ext_MoveTo(-10856, -4263, 0)
		ext_MoveTo(-12075, -7166, 0)
		ext_MoveTo(-13804, -7779, 0)
		waitForMap()

		CommandAll(19957, 12525)
		ext_MoveTilRange(19957, 12525)

		$bossPtr = ext_findBossPtr(4076)
		Attack($bossPtr)

		CancelAll()

		; Wait for boss to die. Restart if your party wipes.
		DO
			IF  ComputeDistance(22166, 11857 , GetMemoryValue($selfPtr, $mAgentX), GetMemoryValue($selfPtr, $mAgentY)) < 300 THEN CONTINUELOOP (2)
			sleep(100)
		UNTIL GetIsDead($bossPtr) = TRUE

		botLooter(TRUE, -1, 0, 0)
	WEND

ENDFUNC