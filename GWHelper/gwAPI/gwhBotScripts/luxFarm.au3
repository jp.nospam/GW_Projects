_ArrayAdd($loadedBotFuncs,"luxFarm")

FUNC luxFarm()
    logGWH("Starting lux farm...")

	While 1
		TravelTo(273, 0)

        IF luxMaxFaction() THEN CONTINUELOOP

        EnterChallenge()
        waitForMap()
        CommandAll(-4010, 5155)
        ext_MoveTo(-4010, 5155,0)
        CallTarget(GetNearestEnemyPtrToAgent($selfPtr))

        $Before = GetLuxonFaction()

        DO
            Sleep(100)
        UNTIL GetLuxonFaction() > $Before
        logGWH("Lux farm wait complete.")

        IF luxMaxFaction() THEN CONTINUELOOP

        CommandAll(-3428, 6753)
        ext_MoveTo(-3428, 6753,0)
        CallTarget(GetNearestEnemyPtrToAgent($selfPtr))

        $Before = GetLuxonFaction()
        logGWH("Lux farm waiting for mob death or killbot...")
        DO
            Sleep(100)
        UNTIL GetLuxonFaction() > $Before

        logGWH("Lux farm wait complete.")
    WEND

    logGWH("Lux farm complete.")
ENDFUNC

#cs
    Function:       luxMaxFaction

    Parameters:
        None

    Return:
        None

    Description:    Decides what to do when max Luxon faction.
#ce
FUNC luxMaxFaction()
    IF GetLuxonFaction() < GetMaxLuxonFaction() THEN RETURN FALSE

	$luxRedeem = int(IniRead($iniBotFile, "Settings", "luxRedeem", "0"))

    TravelTo(193)
    IF $luxRedeem == 0 THEN
        EXIT
    ENDIF

    IF GetDistance(GetNearestNPCPtrToCoords(9065, -1085), $selfPtr) > 2000 THEN
        ext_MoveTo(7573, -1568)
    ENDIF

    GoToNPC(GetNearestNPCPtrToCoords(9065, -1085))

    SWITCH $luxRedeem
    CASE 1
        logGWH("Acquiring jade shards...")
        DO
            Dialog(0x00800101)
            Sleep(100)
        Until GetLuxonFaction() < 5000
        logGWH("jade shard acquisition complete.")
    CASE 2
        logGWH("Acquiring scrolls of the deep...")
        DO
            Dialog(0x00800102)
            Sleep(100)
        UNTIL GetLuxonFaction() < 1000
        logGWH("scroll of the deep acquisition complete.")
    CASE 3
        logGWH("Donating Luxon points...")
        DO
            DonateFaction("Luxon")
            Sleep(100)
        UNTIL GetLuxonFaction() < 5000
        logGWH("Luxon point dontation complete.")
    ENDSWITCH

    RETURN TRUE
ENDFUNC