_ArrayAdd($loadedBotFuncs,"eleEliteBot")

FUNC eleEliteBot()
	IF GetMapID() <> 287 THEN TravelTo(287)

	local $bossPtr = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 287 THEN TravelTo(287)

        Move(27608, 5687, 0)
		waitForMap()

		IF GetDistance(GetNearestEnemyPtrToAgent($selfPtr), $selfPtr)< 1400 THEN
			Move(27615, 5641, 0)
			waitForMap()
			CONTINUELOOP
		ENDIF

		CommandAll(23298, 6425)
		$bossPtr = ext_findBossPtr()

		Move(GetMemoryValue($bossPtr, $mAgentX), GetMemoryValue($bossPtr, $mAgentY))

		Do
			Sleep(100)
		UNTIL GetDistance($bossPtr) < 1250

		$moveX = GetMemoryValue($selfPtr, $mAgentX)
		$moveY = GetMemoryValue($selfPtr, $mAgentY)
		CommandAll($moveX, $moveY)

		CallTarget($bossPtr)
		Move(GetMemoryValue($bossPtr, $mAgentX), GetMemoryValue($bossPtr, $mAgentY))

		DO
			sleep(100)
		UNTIL GetIsDead($bossPtr) = TRUE

		CancelAll()

		botLooter(TRUE, -1, 0, 0)
	WEND
ENDFUNC