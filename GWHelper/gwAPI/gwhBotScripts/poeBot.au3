_ArrayAdd($loadedBotFuncs, "poeBot")

func poeBot()
	LOCAL $maxBagSpace = int(IniRead($iniBotFile, "Settings", "BagSpace", "20"))

	WHILE 1
		IF GetMapID() <> 644 THEN ext_TravelTo(644)

		IF ext_GetBagSpace() < $maxBagSpace Then
			ext_MoveTo(17969, -7345)
			if BagCleaner(17765, -7690, $maxBagSpace) == 1 THEN EXIT
		ENDIF

		logGWH("Grabbing Quest.")
		GoToNPC(GetNearestNPCPtrToCoords(17319, -4861))
		AcceptQuest(856)
		sleep(1000)
		Dialog(0x00000085)
		waitForMap()

		logGWH("Starting run.")
		ext_MoveTo(-16143, -13997)

		if poeFightByCoords(-14573, -15620) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-10882, -16462) > 0 THEN CONTINUELOOP

		ext_MoveTo(-7948, -14999)
		if poeFightByCoords(-7460, -16530) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-3383, -15885) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-1047, -14037) > 0 THEN CONTINUELOOP

		if poeFightByCoords(1180, -14395) > 0 THEN CONTINUELOOP

		if poeFightByCoords(3624, -16685) > 0 THEN CONTINUELOOP


		if poeFightByCoords(6987, -14660) > 0 THEN CONTINUELOOP
		if poeFightByCoords(7865, -16076) > 0 THEN CONTINUELOOP

		ext_MoveTo(9707, -16178)
		if poeFightByCoords(12445, -16063) > 0 THEN CONTINUELOOP

		logGWH("Opening and looting chest.")
		ext_MoveTo(13121, -16045)
		$chest = GetNearestSignpostPtrToCoords(13121, -16045)
		GoToSignpost($chest)
		Sleep(300)
		botLooter(TRUE, -1, 0, 0)

		logGWH("Leaving zone.")
		IF GetMapID() <> 644 THEN TravelTo(644)

		logGWH("Turning in quest.")
		GoToNPC(GetNearestNPCPtrToCoords(17320, -4900))
		QuestReward(856)
		Sleep(500)

		logGWH("Changing regions.")
		$nRegion = -2
		Switch GetRegion()

		Case -2
			$nRegion = 0

		Case 0
			$nRegion = -2

		Case Else
			$nRegion = 0

		EndSwitch
		MoveMap(GetMapID(), $nRegion, 0, GetLanguage())
		waitForMap()
	WEND

ENDFUNC

#cs
    Function:       	poeFightBbyCoords

    Parameters:
		$pX				X value to move to.

		$pY				Y value to move to.

		$fRadius		Radius to search for monsters.

		$fRand			Value to add randomness to coord movement.

    Return:
        0				Successfully made it to coordinates with no enemies.

		1				Map changed.

    Description:    Runs to coordinates and clears all enemies on the way.
#ce
func poeFightByCoords($pX, $pY, $fRadius = 1250, $fRand = 50)

	$mStatus = ext_MoveTilRange($pX, $pY, $fRadius, $fRand)

	WHILE $mStatus = 1
		$tBrawl = GetMemoryValue(GetNearestEnemyPtrToAgent(), $mAgentId)

		$bStatus = brawlTargetById($tBrawl)
		if $bStatus == 2 THEN RETURN 1

		$mStatus = ext_MoveTilRange($pX, $pY, $fRadius, $fRand)
	WEND

	botLooter(TRUE, -1, 0, 0)

	return 0
ENDFUNC

#cs
    Function:       	brawlTargetById

    Parameters:
		$eID			Agent ID to target.

    Return:
        0				Target is dead.

		1				Energy Recharged.

		2				Map change

    Description:		Uses brawling skills against target.
#ce
FUNC brawlTargetById($eID)
	local $intAdrenaline[7] = [0, 0, 0, 100, 250, 175, 0]
	local $target = GetAgentPtr($eID)
	LOCAL $cMap = GetMapID()
	local $sBarPtr = GetSkillBarPtr()
	ChangeTarget($target)
	Attack($target)

	while 1
		If GetMapID() <> $cMap THEN RETURN 2

		; Stand Up
		If GetEnergy() == 0 Then
			$maxEnergy = GetMemoryValue($selfPtr, $mAgentMaxEnergy)
			Do
				If $maxEnergy <> GetMemoryValue($selfPtr, $mAgentEnergyPercent) Then UseSkill(8,$selfPtr)
				Sleep(50)
			Until GetSkillbarSkillRecharge(8, 0, $sBarPtr) <> 0
			Return 1
		EndIf

		; Fight
		For $s = 7 To 2 Step -1
			If GetSkillBarSkillId($s) > 0 AND GetSkillbarSkillRecharge($s, 0, $sBarPtr) == 0 AND $intAdrenaline[$s-1] <= GetSkillbarSkillAdrenaline($s) Then
				UseSkill($s,$target)
				sleep(500)
				ExitLoop
			EndIf
		Next

		if GetIsDead(GetAgentPtr($eID)) OR GetMemoryValue(GetAgentPtr($eID), $mAgentHP) == 0 then return 0
	WEND
ENDFUNC