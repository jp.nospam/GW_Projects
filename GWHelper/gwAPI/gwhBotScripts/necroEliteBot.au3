_ArrayAdd($loadedBotFuncs,"necroEliteBot")

FUNC necroEliteBot()
	IF GetMapID() <> 234 THEN TravelTo(234)

	local $bossPtr = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 234 THEN TravelTo(234)

		; Move through portal
        ext_MoveTo(-6458, 6951, 0)
		waitForMap()

		; Command heroes to point and move to point
		CommandAll(11229, -1170)
		ext_MoveTo(13377, -1498, 0)

		; Find boss
		$bossPtr = ext_findBossPtr()

		; Move until within casting range of boss
		Move(GetMemoryValue($bossPtr, $mAgentX), GetMemoryValue($bossPtr, $mAgentY))
		DO
			Sleep(100)
		UNTIL GetDistance($bossPtr) < 1250


		; Flag heroes within casting range
		$moveX = GetMemoryValue($selfPtr, $mAgentX)
		$moveY = GetMemoryValue($selfPtr, $mAgentY)
		CommandAll($moveX, $moveY)
		CancelAll()

		; Attack boss until it's dead
		Attack($bossPtr)
		DO
			sleep(100)
		UNTIL GetIsDead($bossPtr) = TRUE

		; Loot items
		botLooter(TRUE, -1, 0, 0)
	WEND

ENDFUNC