#include-once

#include "gwhGlobals.au3"
#include "gwAPI.au3"
#include "gwapiExtend.au3"

#include "gwhBotScripts/poeBot.au3"
#include "gwhBotScripts/luxFarm.au3"
#include "gwhBotScripts/necroEliteBot.au3"
#include "gwhBotScripts/eleEliteBot.au3"
#include "gwhBotScripts/ritEliteAltBot.au3"
#include "gwhBotScripts/ritEliteBot.au3"
#include "gwhBotScripts/glintBot.au3"