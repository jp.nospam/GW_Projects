#cs
    Author:     jp.nospam

    Script Function:
        Improve Guild Wars gameplay experience.

    TO-DO:
		1.	Add auto-sell GUI controls/customizability.

        2.  Add Whitelist/blacklist for autoloot.

		3.	Enchantment manager

    Change Log:
		1.2.0.0		Adjusted hero set loading to work better and fixed another
					infinite loop bug.

					Adjusted the way heroes are saved into herosets.

					Fixed $HEROES list to be accurate IDs and names.

		1.1.0.1		Added player info with [Page Up]

					Added Glint Challenge bot farmer

		1.1.0.0		Changed bot implementation.

		1.0.0.1		Fixed heroset loading from infinite looping when it would
					exceed max party size.

		1.0.0.0		Converted to gwAPI.

	Credits:
			@ gamerevision.com
				4D 1		Helped me fix various problems.

				Ralle1976	Contributed on GWA2 version of GW Helper.

				anon777
				mastor		Kilroy bot and some functions that came with it
				kjohn9815

				Baconaise	Luxon faction farm bot
	----------------------------------------------------------------------------
#ce

#NoTrayIcon
#RequireAdmin
#pragma compile(Out, gwhelper.exe)
#pragma compile(ExecLevel, requireAdministrator)
#pragma compile(Compatibility, win7)
#pragma compile(UPX, False)
#pragma compile(ProductVersion, 1.2)
#pragma compile(FileVersion, 1.2.0.0)

; GWH Dependencies
#include <Misc.au3>
#include <Array.au3>
#include <Crypt.au3>
#include "gwAPI.au3"
#include "gwapiExtend.au3"
#include "gwhGlobals.au3"
#include "gwhBots.au3"

; GUI Design/Function
#include <ButtonConstants.au3>
#include <GuiComboBox.au3>
#include <GuiEdit.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <MsgBoxConstants.au3>
#include <ComboConstants.au3>
#include <GUIListBox.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <StringConstants.au3>

; Exit if no process of guild wars exists
IF NOT processexists("gw.exe") THEN EXIT

; Spawn bot instance if called with proper params.
; Parameter 1 - PID, Parameter 2 - bot function
IF $CmdLine[0] > 1 THEN
	DO
		$botting = initialize(int($cmdLine[1]), FALSE)
		Sleep(1000)
	UNTIL $botting = TRUE
	getGWHSettings("all", FALSE)
	$iniBotFile = "gwhBotInfo\" & $CmdLine[2] & ".ini"
	Call($CmdLine[2])
	EXIT
ENDIF

; Use GUI event mode
Opt("GUIOnEventMode", 1)

;==========BEGIN GUI CREATION==========
$gwhForm = GUICreate("GW Helper", 455, 503, 196, 164)
GUISetOnEvent($GUI_EVENT_CLOSE, "gwhFormClose")
GUISetOnEvent($GUI_EVENT_MINIMIZE, "gwhFormMinimize")
GUISetOnEvent($GUI_EVENT_MAXIMIZE, "gwhFormMaximize")
GUISetOnEvent($GUI_EVENT_RESTORE, "gwhFormRestore")
$startbotButton = GUICtrlCreateButton("Start Bot", 16, 176, 75, 25)
GUICtrlSetOnEvent(-1, "startbotButtonClick")
$toggleButton = GUICtrlCreateButton("Toggle", 152, 176, 100, 25)
GUICtrlSetOnEvent(-1, "toggleButtonClick")
$botList = GUICtrlCreateList("", 0, 8, 113, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetOnEvent(-1, "botListChange")
GUICtrlSetData(-1, "")
$toggleList = GUICtrlCreateList("", 144, 8, 121, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "Auto-Loot (OFF)|Name Plates (OFF)")
$utilList = GUICtrlCreateList("", 288, 8, 121, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "Auto-ID [HOME]|Auto-Salvage [END]|Auto-Sell [INSERT]|Call Target [`]|Info [Page Up]")
$Label1 = GUICtrlCreateLabel("Utilities", 328, 184, 37, 17)
$generalTab = GUICtrlCreateTab(8, 232, 433, 257)
$heroSetsSheet = GUICtrlCreateTabItem("Hero Sets")
GUICtrlSetState(-1,$GUI_SHOW)
$heroesList = GUICtrlCreateList("", 150, 289, 121, 160, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "")
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$heroSetLabel = GUICtrlCreateLabel("Hero Set", 40, 265, 64, 25)
GUICtrlSetFont(-1, 12, 400, 0, "Segoe UI")
$heroesLabel = GUICtrlCreateLabel("Heroes", 184, 265, 53, 25)
GUICtrlSetFont(-1, 12, 400, 0, "Segoe UI")
$loadHeroesButton = GUICtrlCreateButton("Load Heros", 152, 457, 115, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "loadHeroesButtonClick")
$newHeroSetButton = GUICtrlCreateButton("Add New Set", 24, 328, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "newHeroSetButtonClick")
$updateHeroesButton = GUICtrlCreateButton("Update Heroes", 280, 328, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "updateHeroesButtonClick")
$removeHeroButton = GUICtrlCreateButton("Remove Hero", 280, 360, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "removeHeroButtonClick")
$setHeroTemplateButton = GUICtrlCreateButton("Set Template", 280, 392, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "setHeroTemplateButtonClick")
$heroSetCombo = GUICtrlCreateCombo("", 16, 296, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "heroSetComboChange")
$removeHeroSet = GUICtrlCreateButton("Remove Set", 24, 360, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "removeHeroSetClick")
$autolootSheet = GUICtrlCreateTabItem("Auto-Loot")
$lootDistanceInput = GUICtrlCreateInput("", 152, 264, 73, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label3 = GUICtrlCreateLabel("Radius:", 96, 264, 40, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label4 = GUICtrlCreateLabel("Enemy To Item Radius:", 32, 296, 112, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$enemyItemRadiusInput = GUICtrlCreateInput("", 152, 296, 121, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$enemyPlayerRadiusInput = GUICtrlCreateInput("", 152, 328, 121, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label5 = GUICtrlCreateLabel("Enemy To Player Radius:", 24, 328, 123, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$autoIDSheet = GUICtrlCreateTabItem("Auto-ID")
$idValueInput = GUICtrlCreateInput("", 144, 264, 121, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
$idWhiteCheckbox = GUICtrlCreateCheckbox("ID White Items", 144, 288, 97, 17)
$Label6 = GUICtrlCreateLabel("Minimum White ID Value:", 16, 264, 123, 17)
$saveAIDButton = GUICtrlCreateButton("Update", 160, 312, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "saveAIDButtonClick")
$autoSalvageSheet = GUICtrlCreateTabItem("Auto-Salvage")
$prefixListbox = GUICtrlCreateList("", 24, 320, 121, 123)
$addPrefixButton = GUICtrlCreateButton("Add Prefix", 24, 303, 121, 17)
GUICtrlSetOnEvent(-1, "addPrefixButtonClick")
$removePrefixButton = GUICtrlCreateButton("Remove Prefix", 24, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removePrefixButtonClick")
$prefixCombo = GUICtrlCreateCombo("", 24, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$addInscriptionButton = GUICtrlCreateButton("Add Inscription", 160, 304, 121, 17)
GUICtrlSetOnEvent(-1, "addInscriptionButtonClick")
$inscriptionCombo = GUICtrlCreateCombo("", 160, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$inscriptionListbox = GUICtrlCreateList("", 160, 320, 121, 123)
$removeInscriptionButton = GUICtrlCreateButton("Remove Inscription", 160, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removeInscriptionButtonClick")
$addSuffixButton = GUICtrlCreateButton("Add Suffix", 296, 304, 121, 17)
GUICtrlSetOnEvent(-1, "addSuffixButtonClick")
$suffixCombo = GUICtrlCreateCombo("", 296, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$suffixListbox = GUICtrlCreateList("", 296, 320, 121, 123)
$removeSuffixButton = GUICtrlCreateButton("Remove Suffix", 296, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removeSuffixButtonClick")
$botsSheet = GUICtrlCreateTabItem("Bots")
$botSettingsCombo = GUICtrlCreateCombo("", 64, 266, 180, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetState(-1, $GUI_DISABLE)
$botSettingsChangeButton = GUICtrlCreateButton("Change", 251, 269, 75, 17)
GUICtrlSetOnEvent(-1, "botSettingsChangeButtonClick")
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetState(-1, $GUI_DISABLE)
$Label2 = GUICtrlCreateLabel("Settings:", 16, 266, 46, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$botEdit = GUICtrlCreateEdit("", 16, 290, 417, 193, BitOR($WS_VSCROLL,$ES_MULTILINE,$ES_READONLY))
GUICtrlSetFont(-1, 10, 400, 0, "Arial")
GUICtrlCreateTabItem("")
;==========END GUI CREATION==========

; Initalize GW Helper
DO
    $gwhLoaded = loadGWH()
     sleep(1000)
UNTIL $gwhLoaded = TRUE

;==========BEGIN CORE FUNCTIONS==========
#cs
    Function:       logGWH

    Parameters:
        $text       Text to write to the log file.

        $fName      Log file name.

    Return:
        None

    Description:    Output text to a log file.
#ce
FUNC logGWH($text = "Unknown log output.", $fName = "gwh.log")
    IF $GWH_LOG = "False" THEN RETURN
    $fHwnd = FileOpen($fName, 1)
    FileWriteLine($fHwnd, @HOUR & ":" & @MIN & @TAB & $text)
    FileClose($fHwnd)
ENDFUNC

#cs
    Function:       getGWHSettings

    Parameters:
		$zone		Which zone of the settings to update.

        $sync		TRUE/FALSE. Decide whether to sync the gui with
					the settings.

    Return:
        None

    Description:    Read INI settings and assigns them to variables.
#ce
FUNC getGWHSettings($zone = "all", $sync = TRUE)
	logGWH($zone & " setting(s) loading.")

	if $zone == "all" Then
		$currentChar = getCharName()
		$iniSettingsSection = "Settings_" & $currentChar
		$iniAutoSalvageSection = "AutoSalvage_" & $currentChar
		$iniHeroSetSection = "HeroSet_" & $currentChar
	ENDIF

    if $zone == "all" or $zone == "HeroSet" THEN $iniHeroSet = inireadsection($iniFile, $iniHeroSetSection)


	; Settings section
	if $zone == "all" or $zone == "autoIDVal" Then $minIDVal = iniread($iniFile, $iniSettingsSection, "autoIDVal", "30")
	if $zone == "all" or $zone == "autoLootDistance" Then $autolootdistance = iniread($iniFile, $iniSettingsSection, "autolootDistance", "1250")
	if $zone == "all" or $zone == "enemyToItem" Then $enemyItemRadius = iniread($iniFile, $iniSettingsSection, "enemyToItem", "850")
	if $zone == "all" or $zone == "enemyToPlayer" Then $enemyPlayerRadius = iniread($iniFile, $iniSettingsSection, "enemyToPlayer", "850")

	if $zone == "all" or $zone == "autoloot" Then
		If iniread($iniFile, $iniSettingsSection, "autoloot", "False") == "True" Then
			$autoloot = TRUE
		Else
			$autoloot = false
		ENDIF
	ENDIF

	if $zone == "all" or $zone == "namePlates" Then
		If iniread($iniFile, $iniSettingsSection, "namePlates", "False") == "True" Then
			$nameToggle = TRUE
		Else
			$nameToggle = false
		ENDIF
	ENDIF

	if $zone == "all" or $zone == "autoIDWhite" Then
		If iniread($iniFile, $iniSettingsSection, "autoIDWhite", "False") == "True" Then
			$idWhite = TRUE
		Else
			$idWhite = false
		ENDIF
	ENDIF

	; Auto-Salvage section
	if $zone == "all" or $zone == "mod1" Then $iniSalvageMod1 = stringsplit(iniread($iniFile, $iniAutoSalvageSection, "mod1", ""), ",")
	if $zone == "all" or $zone == "mod2" Then $iniSalvageMod2 = stringsplit(iniread($iniFile, $iniAutoSalvageSection, "mod2", ""), ",")
	if $zone == "all" or $zone == "inscription" Then $iniSalvageInscr = stringsplit(iniread($iniFile, $iniAutoSalvageSection, "inscription", ""), ",")
	if $zone == "all" or $zone == "salvagables" Then
		If iniread($iniFile, $iniAutoSalvageSection, "salvagables", "False") == "True" Then
			$iniSalvagables = TRUE
		Else
			$iniSalvagables = false
		ENDIF
	ENDIF

	logGWH($zone & " settings loaded.")

    IF $sync == TRUE THEN syncGUI($zone)
ENDFUNC

#cs
    Function:       loadGWH

    Parameters:
		None

    Return:
        True        Successful injection.

        False       Unsucessful injection.
#ce
FUNC loadGWH()
    IF $initHwnd = FALSE THEN $initHwnd = CharacterSelector()
	logGWH("Attemping to inject gwAPI...")
    IF initialize($initHwnd, FALSE, false, false) THEN
		getGWHSettings()
		botKill()
		DO
			IF NOT ProcessExists("gw.exe") THEN EXIT
			Sleep(100)
		UNTIL GetAgentExists(-2)
		$selfPtr = GetAgentPtr(-2)
		DisplayAll($nameToggle)

		WinSetTitle($gwhForm, "", "GW Helper "& FileGetVersion(@AutoItExe) &" - " & $currentChar)
		GUISetState(@SW_SHOW)
		logGWH("gwAPI injected.")
        RETURN TRUE
    ENDIF
	logGWH("gwAPI injection failed.")
    RETURN FALSE
ENDFUNC

#cs
    Function:       syncGUI

    Parameters:
        zone		Which zone of the GUI to update.

    Return:
        None

    Description:    Updates GUI from settings.
#ce
FUNC syncGUI($zone = "all")
    logGWH($zone & " GUI sync start.")

	if $zone == "all" or $zone == "autoloot" THEN
		IF $autoloot == TRUE THEN
			_GUICtrlListBox_ReplaceString($toggleList,0,"Auto-Loot (ON)")
			guictrlsetstate($lootDistanceInput, $GUI_DISABLE)
			guictrlsetstate($enemyItemRadiusInput, $GUI_DISABLE)
			guictrlsetstate($enemyPlayerRadiusInput, $GUI_DISABLE)
		ELSE
			_GUICtrlListBox_ReplaceString($toggleList,0,"Auto-Loot (OFF)")
			guictrlsetstate($lootDistanceInput, $GUI_ENABLE)
			guictrlsetstate($enemyItemRadiusInput, $GUI_ENABLE)
			guictrlsetstate($enemyPlayerRadiusInput, $GUI_ENABLE)
		ENDIF
	ENDIF

	if $zone == "all" or $zone == "namePlates" THEN
		IF $nameToggle == TRUE THEN
			_GUICtrlListBox_ReplaceString($toggleList,1,"Name Plates (ON)")
		ELSE
			_GUICtrlListBox_ReplaceString($toggleList,1,"Name Plates (OFF)")
		ENDIF
	ENDIF

	if $zone == "all" or $zone == "autoIDWhite" THEN
		IF $idWhite == TRUE THEN
			GUICtrlSetState($idWhiteCheckbox, 1)
		ELSE
			GUICtrlSetState($idWhiteCheckbox, 4)
		ENDIF
	ENDIF


	if $zone == "all" or $zone == "autoIDVal" THEN GuiCtrlSetData($idValueInput, $minIdVal)

	if $zone == "all" or $zone == "autoLootDistance" THEN guictrlsetdata($lootDistanceInput, $autolootdistance)

	if $zone == "all" or $zone == "enemyToItem" THEN guictrlsetdata($enemyItemRadiusInput, $enemyItemRadius)

	if $zone == "all" or $zone == "enemyToPlayer" THEN guictrlsetdata($enemyPlayerRadiusInput, $enemyPlayerRadius)

	if $zone == "all" or $zone == "HeroSet" THEN
		GuiCtrlSetState($updateHeroesButton,$GUI_DISABLE)
		GuiCtrlSetState($removeHeroButton,$GUI_DISABLE)
		GuiCtrlSetState($setHeroTemplateButton,$GUI_DISABLE)
		GuiCtrlSetState($removeHeroSet,$GUI_DISABLE)
		GuiCtrlSetState($loadHeroesButton,$GUI_DISABLE)
		GUICtrlSetData($heroSetCombo, "")
		guictrlsetdata($heroesList, "")
		IF IsArray($iniHeroSet) Then
			FOR $x = 1 TO $iniHeroSet[0][0]
				_GUICtrlComboBox_AddString($heroSetCombo, $iniHeroSet[$x][0])
			NEXT
		ENDIF
	endif

	if $zone == "all" or $zone == "BotList" THEN
	GUICtrlSetData($botList, "")
		IF IsArray($loadedBotFuncs) and UBound($loadedBotFuncs) >0 Then
			FOR $x = 0 TO (UBound($loadedBotFuncs) - 1)
				$bFile = "gwhBotInfo\" & $loadedBotFuncs[$x] & ".ini"
				_GUICtrlListBox_AddString($botList, iniRead($bFile, "gwh", "Description", "Untitled Bot"))
			NEXT
		ENDIF
	endif

	if $zone == "all" or $zone == "mod1" THEN
		GuiCtrlSetData($prefixCombo, "")
		GUICtrlSetData($prefixListbox, "")
		For $p = 0 TO UBound($prefixList) - 1
			if _ArraySearch($iniSalvageMod1, $prefixList[$p][0]) == -1 Then
				_GUICtrlComboBox_AddString($prefixCombo,$prefixList[$p][0])
			ELSE
				_GUICtrlListBox_AddString($prefixListbox,$prefixList[$p][0])
			ENDIF
		NEXT

		if _GUICtrlComboBox_GetCount($prefixCombo) > 0 THEN
			_GUICtrlComboBox_SetCurSel($prefixCombo,0)
			GUIctrlsetstate($addPrefixButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($addPrefixButton, $GUI_DISABLE)
		endif

		if _GUICtrlListBox_GetCount($prefixListbox) > 0 THEN
			GUIctrlsetstate($removePrefixButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($removePrefixButton, $GUI_DISABLE)
		endif
	endif

	if $zone == "all" or $zone == "inscription" THEN
		GuiCtrlSetData($inscriptionCombo, "")
		guictrlsetdata($inscriptionListbox, "")
		For $p = 0 TO UBound($inscrList) - 1
			if _ArraySearch($iniSalvageInscr, $inscrList[$p][0]) == -1 Then
				_GUICtrlComboBox_AddString($inscriptionCombo,$inscrList[$p][0])
			ELSE
				_GUICtrlListBox_AddString($inscriptionListbox,$inscrList[$p][0])
			ENDIF
		NEXT

		if _GUICtrlComboBox_GetCount($inscriptionCombo) > 0 THEN
			_GUICtrlComboBox_SetCurSel($inscriptionCombo,0)
			GUIctrlsetstate($addInscriptionButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($addInscriptionButton, $GUI_DISABLE)
		endif

		if _GUICtrlListBox_GetCount($inscriptionListbox) > 0 THEN
			GUIctrlsetstate($removeInscriptionButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($removeInscriptionButton, $GUI_DISABLE)
		endif
	endif

	if $zone == "all" or $zone == "mod2" THEN
		GuiCtrlSetData($suffixCombo, "")
		GuiCtrlSetData($suffixListbox, "")
		For $p = 0 TO UBound($suffixList) - 1
			if _ArraySearch($iniSalvageMod2, $suffixList[$p][0]) == -1 Then
				_GUICtrlComboBox_AddString($suffixCombo,$suffixList[$p][0])
			ELSE
				_GUICtrlListBox_AddString($suffixListbox,$suffixList[$p][0])
			ENDIF
		NEXT

		if _GUICtrlComboBox_GetCount($suffixCombo) > 0 THEN
			_GUICtrlComboBox_SetCurSel($suffixCombo,0)
			GUIctrlsetstate($addSuffixButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($addSuffixButton, $GUI_DISABLE)
		endif

		if _GUICtrlListBox_GetCount($suffixListbox) > 0 THEN
			GUIctrlsetstate($removeSuffixButton, $GUI_ENABLE)
		else
			GUIctrlsetstate($removeSuffixButton, $GUI_DISABLE)
		endif
	endif

	logGWH($zone & " GUI sync complete.")
ENDFUNC

#cs
    Function:       addHeroes

    Parameters:
        $hList      	List of hero id's to add. Delimited by commas.
						Uses $hList[0] as the max index of array.

		$useTemplates	True/False. Use templates when loading heroes.
	Return:
        None

    Description:    Adds list of heroes to your party.
#ce
FUNC addHeroes($hList = FALSE, $useTemplates = false)
	local $iniCat = 0

    IF $hList == FALSE THEN RETURN

	if $useTemplates <> FALSE THEN $iniCat = IniReadSection($iniFile, $useTemplates)

    $hList = stringsplit($hList, ",")
    logGWH("Adding heroes to party...")
    FOR $x = 1 TO $hList[0]
        $hID = _ArraySearch($HEROES, $hList[$x])
        $hNum = GetHeroNumberByHeroId($hID)
        IF $hID > -1 AND $hNum < 1 THEN
			AddHero($hID)
			sleep(500)
		EndIF
		$hNum = GetHeroNumberByHeroId($hID)
		IF $hNum < 1 THEN
			WriteChat("Unable to load " & $HEROES[$hID] & ".", "GW Helper")
			CONTINUELOOP
		ENDIF
		$hFind = _ArraySearch($iniCat, $HEROES[$hID], 0, 0, 0, 0, 0, 0)
		IF IsArray($iniCat) AND $useTemplates <> FALSE AND $hFind <> -1 THEN
			$tfName = $iniCat[$hFind][1]
			if FileExists($tfName) Then
				$tLoad = 0
				$sfHwnd = FileOpen($tfName)
				$tLoad = FileReadLine($sfHwnd)
				FileClose($sfHwnd)
				if IsString($tLoad) THEN
					ext_LoadSkillTemplate($tLoad, $hNum)
				ENDIF
			ENDIF
		endif
    NEXT
    logGWH("Hero party addition complete.")
ENDFUNC

#cs
    Function:       waitForMap

    Parameters:
		None

    Return:
        None

    Description:	Pauses script waiting for map to load.
#ce
FUNC waitForMap()
	logGWH("Waiting for load state...")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetMapLoading() = 2

	logGWH("Waiting for map to load.")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetMapLoading() <> 2
	logGWH("Map loaded.")

	logGWH("Waiting for agent to load.")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetAgentExists(-2)
	logGWH("Agent loaded.")
	$selfPtr = GetAgentPtr(-2)

    IF $nameToggle == TRUE THEN DisplayAll($nameToggle)

	IF $CmdLine[0] > 1 THEN RETURN

	logGWH("Getting character name.")
	IF $currentChar <> getCharName() THEN
		getGWHSettings()
		WinsetTitle($gwhForm, "", "GW Helper - " & $currentChar)
	ENDIF
ENDFUNC

#cs
    Function:       botKill

    Parameters:
		None

    Return:
		None
#ce
FUNC botKill()
	$killBot = iniread($iniFile, $iniSettingsSection, "botPID", "")
	IF int($killBot) <= 0 OR NOT ProcessExists($killBot) THEN
		iniwrite($iniFile, $iniSettingsSection, "botPID", "")
		RETURN
	ENDIF
	ProcessClose($killBot)
	iniwrite($iniFile, $iniSettingsSection, "botPID", "")
ENDFUNC
;==========END CORE FUNCTIONS==========

#cs
    Function:       idAll

    Parameters:
		$idWhite	Boolean value that determines if it should identify white items

		$idVal		INT value to determine at what price to identify white items

    Return:
        None

    Description:    ID's all items in every bag.
#ce
FUNC idAll($idWhite = FALSE, $whiteVal = 30)
	WriteChat("ID starting...", "GW Helper")
	ext_IdentifyBag(1, $idWhite, $whiteVal)
	ext_IdentifyBag(2, $idWhite, $whiteVal)
	ext_IdentifyBag(3, $idWhite, $whiteVal)
	ext_IdentifyBag(4, $idWhite, $whiteVal)
	ext_IdentifyBag(5, $idWhite, $whiteVal)
	if FindIDKit() == 0 THEN
		WriteChat("ID incomplete: Ran out of ID kits.", "GW Helper")
		return 0
	ELSE
		WriteChat("ID complete.", "GW Helper")
		return 1
	ENDIF
ENDFUNC

#cs
    Function:       sellAll

    Parameters:
		None

    Return:
        None

    Description:    Sells items in first 3 bags.
#ce
FUNC sellAll()
	WriteChat("Selling...", "GW Helper")
	sellBag(1)
	sellBag(2)
	sellBag(3)
	WriteChat("Sell complete.", "GW Helper")
ENDFUNC

#cs
    Function:       	sellCheck

    Parameters:
		$iObj			Item object to check.

    Return:
        None

    Description:   		Checks items to sell.
#ce
Func sellCheck($iObj)
	local $noSellType[] = [8,9,10,11,18,21,29,30,31,43,45]

	$iType = GetMemoryValue($iObj, $mItemType)

	for $x = 0 to ubound($noSellType) - 1
		if $noSellType[$x] == int($iType) THEN RETURN FALSE
	Next
	RETURN TRUE
ENDFUNC

#cs
    Function:       	sellBag

    Parameters:
		$bagIndex		Index of bag to sell.

    Return:
        None

    Description:    	Sell items in a bag.
#ce
Func sellBag($bagIndex)
	$bag = GetBagPtr($bagIndex)
	$numOfSlots = GetMemoryValue($bag, $mBagSlots)
	For $i = 1 To $numOfSlots
		$item = GetItemPtrBySlot($bag, $i)
		if GetMemoryValue($item, $mItemId) == 0 THEN ContinueLoop
		If sellCheck($item) == True Then
			SellItem($item)
			Sleep(250)
		EndIf
	Next
EndFunc

#cs
    Function:       	salvageAll

    Parameters:
		$salv			Decides whether to salvage salvageable items.

    Return:
        None

    Description:    	Salvage all items in first 3 bags.
#ce
FUNC salvageAll($salv = FALSE)
	WriteChat("Salvaging...", "GW Helper")
	salvageBag(1, $salv)
	salvageBag(2, $salv)
	salvageBag(3, $salv)
	WriteChat("Salvage complete.", "GW Helper")
ENDFUNC

#cs
    Function:       	salvageBag

    Parameters:
		$bagIndex		Index of bag to salvage.

		$salvagables	Decides whether to salvage salvageable items.

    Return:
        None

    Description:    	Salvage all items in a bag.
#ce
FUNC salvageBag($bagIndex, $salvagables = FALSE)
	$bag = GetBagPtr($bagIndex)
	$numOfSlots = GetMemoryValue($bag, $mBagSlots)

	For $i = 1 To $numOfSlots
		$item = GetItemPtrBySlot($bag, $i)
		if GetMemoryValue($item, $mItemId) == 0 OR GetMemoryValue($item, $mItemType) == 8 THEN ContinueLoop

		For $x = 3 To 0 Step - 1
			$sCheck = salvageCheck($item, $salvagables, $x)
			If $sCheck > -1 Then
				StartSalvage($item)
				sleep(500)
				if $sCheck == 3 THEN
					SalvageMaterials()
				ELSE
					SalvageMod($sCheck)
				Endif
				Sleep(250)
			EndIf
		Next
	Next

ENDFUNC

#cs
    Function:       	salvageCheck

    Parameters:
		$iObj			Item object/struct

		$salvagables	Decides whether to salvage salvageable items.

		$modCheck		Which modification to check to see if it should be salvaged.
						Refer to return values to adjust this parameter to match the
						type of check you want.

    Return:
		-1				Item should not be salvaged.

		0				Item prefix should be salvaged

		1				Item suffix should be salvaged.

		2				Item inscription should be salvaged.

        3				Item is a slavageable and should be salvaged.

    Description:    	Checks to see if an item's mod should be salvaged.
#ce
FUNC salvageCheck($iObj, $salvagables = FALSE, $modCheck = 3)
	If  GetMemoryValue($iObj, $mItemType) == 0 AND $salvagables == TRUE AND $modCheck == 3 THEN RETURN 3

	$modStruct = string(GetModStruct($iObj))

	if $iniSalvageMod2[0] >= 1 AND $modCheck == 1 AND $iniSalvageMod2[1] <> "" Then
		For $m = 1 TO $iniSalvageMod2[0]
			$val = _ArraySearch($suffixList, $iniSalvageMod2[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $suffixList[$val][1]) <> 0 THEN RETURN 1
			endif
		Next
	ENDIF

	if $iniSalvageMod1[0] >= 1 AND $modCheck == 0 AND $iniSalvageMod1[1] <> "" Then
		For $m = 1 TO $iniSalvageMod1[0]
			$val = _ArraySearch($prefixList, $iniSalvageMod1[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $prefixList[$val][1]) <> 0 THEN RETURN 0
			endif
		Next
	ENDIF

	if $iniSalvageInscr[0] >= 1 AND $modCheck == 2 AND $iniSalvageInscr[1] <> "" Then
		For $m = 1 TO $iniSalvageInscr[0]
			$val = _ArraySearch($inscrList, $iniSalvageInscr[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $inscrList[$val][1]) <> 0 THEN RETURN 2
			endif
		Next
	ENDIF

	RETURN -1
ENDFUNC

#cs
    Function:			lootItems

    Parameters:
        $itemToPlayer   Max radius it will search for dropped items.

        $itemToEnemy    Radius to search for enemies around the item.
                        Will NOT loot if an enemy is within radius to item.

        $enemyToPlayer  Radius to search for enemies around the player.
                        Will NOT loot if an enemy is within radius to the player.

    Return:
        0				Unable to pickup item because game/player is busy.

		1				No items to pickup.

		2				Item out of loot radius.

		3				Enemy near item or player.

		4				Attempting to pick up item.

    Description:		Automatically loots dropped items.
#ce
FUNC lootItems($itemToPlayer = 850, $itemToEnemy = 850 ,$enemyToPlayer = 850)

	IF GetMapLoading() <> 1 OR GetAgentPtr(-1) <> 0 OR GetIsMoving($selfPtr) == TRUE OR ext_GetIsCasting($selfPtr) == TRUE THEN RETURN 0

    $tItem = ext_GetNearestItemPtrToPlayer()

    IF NOT IsPtr($tItem) OR GetIsMovable($tItem) == FALSE OR ext_GetBagSpace() == 0 THEN RETURN 1

    $cDist = getDistance($tItem)
    IF $cDist > $itemToPlayer THEN RETURN 2

    $itemEnemy = GetNearestEnemyPtrToAgent($tItem)
    $selfEnemy = GetNearestEnemyPtrToAgent()

    if GetDistance($tItem, $itemEnemy) < $itemToEnemy OR GetDistance($selfEnemy, -2) < $enemyToPlayer  THEN RETURN 3

    Move(GetMemoryValue($tItem, $mAgentX), GetMemoryValue($tItem, $mAgentY))

    IF $cDist < 50 THEN
        PickUpItem($tItem)
        sleep(800)
    ENDIF

	RETURN 4
ENDFUNC

#cs
    Function:       getItemModStructDiff

    Parameters:
        None

    Return:
        None

    Description:    Gets the mod struct difference between bag 1 slot 1 (plain)
					and bag 1 slot 2 (modded) items.
					Use in PvP to get struct values for salvager.
					Outputs to modstruct.txt
#ce
FUNC getItemModStructDiff()
	$item1 = GetItemPtrBySlot(1,1)
	$item2 = GetItemPtrBySlot(1,2)

	$s1 = string(GetModStruct($item1))
	$s2 = string(GetModStruct($item2))

	$mName = inputbox("Name?", "Name?")

	$mFwnd = fileopen("modstruct.txt", 1)
	FileWrite($mFwnd, $mName & " : ")
	For $x = 1 To StringLen($s1)
		If StringMid($s1,$x,1) <> StringMid($s2,$x,1) Then
			$rTrim = StringLen($s1) - $x
			$s2 = stringtrimleft($s2, $x + 7)
			$s2 = stringtrimright($s2, $rTrim + 1)
			FileWrite($mFwnd, $s2 & @CRLF & @CRLF)
			exitloop
		EndIf
	Next
	fileclose($mFwnd)
ENDFUNC

FUNC gwh_Main()
	IF NOT processexists("gw.exe") THEN EXIT

	if GetMapLoading() == 2 THEN waitForMap()

	; Autolooter
	IF $autoloot == TRUE THEN lootItems($autolootdistance, $enemyItemRadius, $enemyPlayerRadius)

	; Salvage Bags
	if _IsPressed(23) Then
		WHILE  _IsPressed(23)
			sleep(100)
		WEND
		salvageAll($iniSalvagables)
	ENDIF

	; ID Bags
    IF _IsPressed(24) THEN
		WHILE  _IsPressed(24)
			sleep(100)
		WEND
		idAll($idWhite, $minIDVal)
	ENDIF

	; Sell Bags
	if _IsPressed("2D") Then
		WHILE  _IsPressed("2D")
			sleep(100)
		WEND
		sellAll()
		;getItemModStructDiff()
	ENDIF

	; Call Target
	IF _IsPressed("C0") AND GetAgentPtr(-1) <> 0 THEN
		WHILE _IsPressed("C0")
			sleep(100)
		WEND
		CallTarget(GetAgentPtr(-1))
	ENDIF

	; Call Target
	IF _IsPressed(21) <> 0 THEN
		WHILE _IsPressed(21)
			sleep(100)
		WEND
		If IsPtr($selfPtr) Then
			WriteChat("Self - X: " & GetMemoryValue($selfPtr, $mAgentX) & " Y: " & GetMemoryValue($selfPtr, $mAgentY),"GW Helper")
		ENDIF

		$myTarget = GetAgentPtr(-1)
		If IsPtr($myTarget) Then
			WriteChat("Target - Player Number/Model ID: " & GetMemoryValue($myTarget, $mAgentPlayerNumber) & " X: " & GetMemoryValue($myTarget, $mAgentX) & " Y: " & GetMemoryValue($myTarget, $mAgentY) & _
			" Type: " & GetMemoryValue($myTarget, $mAgentType), "GW Helper")
			WriteChat("Distance: " & GetDistance($selfPtr, $myTarget), "GW Helper")
		EndIf
		WriteChat("----", "GW Helper")


	ENDIF
ENDFUNC

WHILE 1
	gwh_Main()
    SLEEP(100)
WEND

;==========START GUI EVENT FUNCTIONS==========
Func gwhFormClose()
	Exit
EndFunc

Func gwhFormMaximize()
EndFunc

Func gwhFormMinimize()
EndFunc

Func gwhFormRestore()
EndFunc

FUNC saveAIDButtonClick()
	$tIDVal = GUICtrlRead($idValueInput)
	$tWhite = GuiCtrlRead($idWhiteCheckbox)

	IF $tIDVal < 1 THEN $tIDVal = 0

	IF $tWhite == 1 THEN
		$tWhite = "True"
	ELSE
		$tWhite = "False"
	ENDIF

	IniWrite($iniFile, $iniSettingsSection, "autoIDWhite", $tWhite)
	IniWrite($iniFile, $iniSettingsSection, "autoIDval", $tIDVal)
	getGWHSettings("autoIDWhite", false)
	getGWHSettings("autoIDVal", false)
ENDFUNC

FUNC newHeroSetButtonClick()
	$iBoxHeroSet = inputbox("HeroSet Name?", "Please input a new hero set name.")
	IF $iBoxHeroSet <> "" THEN
		iniwrite($iniFile, $iniHeroSetSection, $iBoxHeroSet, "")
	ENDIF
	getGWHSettings("HeroSet")
	_GUICtrlComboBox_SetCurSel($herosetCombo, _GUICtrlComboBox_GetCount($heroSetCombo) - 1)
	heroSetComboChange()
ENDFUNC

Func updateHeroesButtonClick()
	$cPartySize = GetPartySize()
	IF $cPartySize < 2 THEN RETURN
	local $chList

	For $x = 1 TO $cPartySize
		For $i = 1 To UBound($HEROES) - 1
			IF GetHeroNumberByHeroId($i) == $x THEN
				$chList = $chList & $HEROES[$i] & ","
				ExitLoop
			ENDIF
		Next
	NEXT

	$chList =StringTrimRight($chList, 1)

	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo) + 1

	iniWrite($iniFile, $iniHeroSetSection, $iniHeroSet[$curSel][0],$chList)

	getGWHSettings("HeroSet")
	_GUICtrlComboBox_SetCurSel($heroSetCombo, $curSel - 1)
	heroSetComboChange()
EndFunc

Func removeHeroButtonClick()
	IF _GUICtrlListBox_GetCurSel($heroesList) == -1 THEN RETURN

	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo) + 1
	$hsList = ""
	$listCount = _GUICtrlListBox_GetCount($heroesList)
	$rHero = GuiCtrlRead($heroesList)

	IF $listCount > 1 Then

		$nList = StringSplit($iniHeroSet[$curSel][1],",")
		$rHero = _ArraySearch($nList, $rHero)
		FOR $x = 1 TO $nList[0]
			IF $x == $rHero THEN CONTINUELOOP
			$hsList = $hsList & $nList[$x] & ","
		NEXT

		$hsList = StringTrimRight($hsList, 1)

	ENDIF

	IniWrite($iniFile, $iniHeroSetSection, $iniHeroSet[$curSel][0], $hsList)
	getGWHSettings("HeroSet")
	_GUICtrlComboBox_SetCurSel($heroSetCombo, $curSel - 1)
	heroSetComboChange()
EndFunc

func setHeroTemplateButtonClick()
	$cSel = _GUICtrlListBox_GetCurSel($heroesList)
	IF $cSel == -1 THEN RETURN

	$sFolder = @MyDocumentsDir & "\Guild Wars\Templates\Skills"
	if FileExists($sFolder) == 0 THEN $sFolder = @WindowsDir

	$tKey = _GuiCtrlListBox_GetText($heroesList, $cSel)
	$tsFile = FileOpenDialog("Template for "& $tKey, $sFolder, "Templates (*.txt)", 1)

	If not FileExists($tsFile) THEN RETURN

	$tSection = $currentChar&GUICtrlRead($heroSetCombo)
	$tSection = hex(_Crypt_Hashdata($tSection,$CALG_MD5))

	iniWrite($iniFile, $tSection, $tKey, $tsFile)
ENDFUNC

Func removeHeroSetClick()
	IniDelete($iniFile, $iniHeroSetSection, GUICtrlRead($herosetCombo))
	$tSection = $currentChar&GUICtrlRead($heroSetCombo)
	$tSection = hex(_Crypt_Hashdata($tSection,$CALG_MD5))
	IniDelete($iniFile, $tSection)
	getGWHSettings("HeroSet")
	heroSetComboChange()
	GuiCtrlSetData($heroesList, "")
EndFunc

Func loadHeroesButtonClick()
    $curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo)

	IF $curSel = -1 OR _GUICtrlListBox_GetCount($heroesList) < 1 THEN RETURN

	$tSet = $currentChar&GUICtrlRead($heroSetCombo)
	$tSet = hex(_Crypt_Hashdata($tSet,$CALG_MD5))

    addHeroes($iniHeroSet[$curSel + 1][1], $tSet)
EndFunc

Func heroSetComboChange()
	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo)
	IF $curSel = -1 THEN RETURN

	GuiCtrlSetState($updateHeroesButton,$GUI_ENABLE)
	GuiCtrlSetState($removeHeroSet,$GUI_ENABLE)
	GuiCtrlSetState($loadHeroesButton,$GUI_ENABLE)

    $cursel += 1
    $hList = stringsplit($iniHeroSet[$curSel][1], ",")
    GUICtrlSetData($heroesList, "")

    logGWH("Populating Heroes List...")
	$added = -1
    FOR $x = 1 TO $hList[0]
		IF $hList[$x] == "" THEN CONTINUELOOP
        $added = _GUICtrlListBox_AddString($heroesList, $hList[$x])
    NEXT
	if $added <> -1 THEN
		GuiCtrlSetState($removeHeroButton,$GUI_ENABLE)
		GuiCtrlSetState($setHeroTemplateButton,$GUI_ENABLE)
	ENDIF
    logGWH("Heroes list population complete.")
Endfunc

Func startbotButtonClick()
	$selectedBot = _GUICtrlListBox_GetCurSel($botList)

	If $selectedBot == -1 Then RETURN

	$killBot = ShellExecute(@ScriptName, $initHwnd & " " & $loadedBotFuncs[$selectedBot])
	IniWrite($iniFile, $iniSettingsSection, "botPID", $killBot)
	Exit
EndFunc

FUNC botListChange()
	GUICtrlSetData($botSettingsCombo, "")
	GuiCtrlSetState($botSettingsChangeButton, $GUI_DISABLE)
	GUICtrlSetState($botSettingsCombo, $GUI_DISABLE)
	$selectedBot = _GUICtrlListBox_GetCurSel($botList)

	If $selectedBot == -1 Then RETURN

	GuiCtrlSetState($botsSheet, $GUI_SHOW)

	$botFunc = $loadedBotFuncs[$selectedBot]

	$botFuncSettings = inireadsection("gwhBotInfo\" & $botFunc & ".ini", "Settings")

	If IsArray($botFuncSettings) Then

		For $k = 1 To $botFuncSettings[0][0]
			_GUICtrlComboBox_AddString($botSettingsCombo, $botFuncSettings[$k][0] & " (" & $botFuncSettings[$k][1] & ")")
		Next

		GuiCtrlSetState($botSettingsChangeButton, $GUI_ENABLE)
		GUICtrlSetState($botSettingsCombo, $GUI_ENABLE)
		_GuiCtrlComboBox_SetCurSel($botSettingsCombo, 0)
	EndIf

	$botDescFile=  @ScriptDir & "\gwhBotInfo\" &  $botFunc & ".txt"

	If FileExists($botDescFile) Then
		GuiCtrlSetData($botEdit, "")
		local $descArray = 0
		$botDescHwnd = FileOpen($botDescFile)
		$descArray = FileReadToArray($botDescHwnd)
		FileClose($botDescHwnd)
		$maxLines = Ubound($descArray) - 1
		For $x = 0 To $maxLines
			$text = $descArray[$x]
			if $x <> $maxLines THEN $text = $text & @CRLF
			_GUICtrlEdit_InsertText($botEdit, $text)
		NEXT
	ENDIF
ENDFUNC

Func botSettingsChangeButtonClick()
	$selectedBotSetting = _GUICtrlComboBox_GetCurSel($botSettingsCombo)

	If $selectedBotSetting == -1 THEN Return

	$selectedBot = _GUICtrlListBox_GetCurSel($botList)

	If $selectedBot == -1 Then RETURN

	$botFunc = $loadedBotFuncs[$selectedBot]

	$botFuncSettings = inireadsection("gwhBotInfo\" & $botFunc & ".ini", "Settings")

	$botKey = $botFuncSettings[$selectedBotSetting + 1][0]

	$botValue = InputBox("New Value", "Please enter a new value for: " & $botKey)

	iniWrite("gwhBotInfo\" & $botFunc & ".ini", "Settings", $botKey, $botValue)

	botListChange()
	_GuiCtrlComboBox_SetCurSel($botSettingsCombo, $selectedBotSetting)
EndFunc

Func toggleButtonClick()
	$select = 0
    SWITCH _GUICtrlListBox_GetCurSel($toggleList)
    CASE 0
		IF $autoloot == TRUE THEN
			$autoloot = FALSE
		ELSE
			$autoloot = TRUE
			iniwrite($iniFile, $iniSettingsSection, "autolootdistance", GUICtrlRead($lootDistanceInput))
			iniwrite($iniFile, $iniSettingsSection, "enemyToItem", GUICtrlRead($enemyItemRadiusInput))
			iniwrite($iniFile, $iniSettingsSection, "enemyToPlayer", GUICtrlRead($enemyPlayerRadiusInput))
		ENDIF

		iniwrite($iniFile, $iniSettingsSection, "autoloot", $autoloot)
		getGWHSettings("autoloot")
		getGWHSettings("autolootDistance")
		getGWHSettings("enemyToItem")
		getGWHSettings("enemyToPlayer")
    CASE 1
		IF $nameToggle == TRUE THEN
			$nameToggle = FALSE
		ELSE
			$nameToggle = TRUE
		ENDIF
		iniwrite($iniFile, $iniSettingsSection, "namePlates", $nameToggle)
		DisplayAll($nameToggle)
        $select = 1
		getGWHSettings("namePlates")
    ENDSWITCH
	_GuiCtrlListBox_SetCurSel($toggleList, $select)
EndFunc

func addPrefixButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($prefixCombo)

	IF $curSel < 0 THEN RETURN

	$cPrefixes = _GUICtrlListBox_GetCount($prefixListbox)
	if $cPrefixes < 1 Then
		iniwrite($iniFile, $iniAutoSalvageSection, "mod1", GuiCtrlRead($prefixCombo))
	Else
		$t = ""
		For $p = 0 TO $cPrefixes - 1
			$t = $t & _GUICtrlListBox_GetText($preFixListBox, $p) & ","
		NEXT
		iniwrite($iniFile, $iniAutoSalvageSection, "mod1", $t & GuiCtrlRead($prefixCombo))
	ENDIF

	getGWHSettings("mod1")
Endfunc

func removePrefixButtonClick()
	$cPrefix = _GUICtrlListBox_GetCurSel($prefixListBox)

	IF $cPrefix < 0 THEN Return

	$tPrefixes = _GUICtrlListBox_GetCount($prefixListBox)
	$nPrefixes = ""
	For $p = 0 TO $tPrefixes - 1
		if $p <> $cPrefix THEN $nPrefixes = $nPrefixes & _GUICtrlListBox_GetText($prefixListBox, $p) & ","
	NExt
	$nPrefixes = StringTrimRight($nPrefixes, 1)

	iniwrite($iniFile, $iniAutoSalvageSection, "mod1", $nPrefixes)

	getGWHSettings("mod1")
Endfunc

func addInscriptionButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($inscriptionCombo)

	IF $curSel < 0 THEN RETURN

	$cInscriptions = _GUICtrlListBox_GetCount($inscriptionListbox)
	if $cInscriptions < 1 Then
		iniwrite($iniFile, $iniAutoSalvageSection, "inscription", GuiCtrlRead($inscriptionCombo))
	Else
		$t = ""
		For $p = 0 TO $cInscriptions - 1
			$t = $t & _GUICtrlListBox_GetText($inscriptionListbox, $p) & ","
		NEXT
		iniwrite($iniFile, $iniAutoSalvageSection, "inscription", $t & GuiCtrlRead($inscriptionCombo))
	ENDIF

	getGWHSettings("inscription")
Endfunc

func removeInscriptionButtonClick()
	$cInscriptions = _GUICtrlListBox_GetCurSel($inscriptionListBox)

	IF $cInscriptions < 0 THEN Return

	$tInscriptions = _GUICtrlListBox_GetCount($inscriptionListBox)
	$nInscriptions = ""
	For $p = 0 TO $tInscriptions - 1
		if $p <> $cInscriptions THEN $nInscriptions = $nInscriptions & _GUICtrlListBox_GetText($inscriptionListBox, $p) & ","
	Next
	$nInscriptions = StringTrimRight($nInscriptions, 1)

	iniwrite($iniFile, $iniAutoSalvageSection, "inscription", $nInscriptions)

	getGWHSettings("inscription")
EndFunc

func addSuffixButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($suffixCombo)

	IF $curSel < 0 THEN RETURN

	$cSuffixes = _GUICtrlListBox_GetCount($suffixListbox)
	if $cSuffixes < 1 Then
		iniwrite($iniFile, $iniAutoSalvageSection, "mod2", GuiCtrlRead($suffixCombo))
	Else
		$t = ""
		For $p = 0 TO $cSuffixes - 1
			$t = $t & _GUICtrlListBox_GetText($suffixListBox, $p) & ","
		NEXT
		iniwrite($iniFile, $iniAutoSalvageSection, "mod2", $t & GuiCtrlRead($suffixCombo))
	ENDIF

	getGWHSettings("mod2")
EndFunc

func removeSuffixButtonClick()
	$cSuffix = _GUICtrlListBox_GetCurSel($suffixListBox)

	IF $cSuffix < 0 THEN Return

	$tSuffixes = _GUICtrlListBox_GetCount($suffixListBox)
	$nSuffixes = ""
	For $p = 0 TO $tSuffixes - 1
		if $p <> $cSuffix THEN $nSuffixes = $nSuffixes & _GUICtrlListBox_GetText($suffixListBox, $p) & ","
	NExt
	$nSuffixes = StringTrimRight($nSuffixes, 1)

	iniwrite($iniFile, $iniAutoSalvageSection, "mod2", $nSuffixes)

	getGWHSettings("mod2")
endfunc
;==========END GUI EVENT FUNCTIONS==========