	 _________________
	|TABLE OF CONTENTS|

	I. About Gw Helper

	II. Credits and Contributions

	III. Known Bugs

	IV. Reporting problems/bugs/glitches.

	V. End-User bot adding.

	VI. Adding/Writing your own bot
		BD.i Debugging
		BD.ii Adding your own settings
		BD.iii Usable globals
		BD.iv Documentation

	1. Botting
	2. Autoloot
	3. Nameplates
	4. Auto-ID
	5. Hero Sets
	6. Auto-Sell
	7. Auto-Salvage

------------------
I. About GW Helper
------------------
CHANGELOG: View gwhelper.au3

This script is designed to be a "helper" more than a botting tool.
Although I intend to include more bots and botting scripts reside within it, adding/fixing "utilities" will
always be my priority over adding bots.

The script MUST BE compiled into an EXE or the bots won't work unless you know how to set parameters for SCiTE.
Parameters are: 1 - Guild wars PID, 2 - bot function name

Guild Wars must be running before the helper is ran.

-----------------------------
II. Credits and Contributions
-----------------------------
Any contributions or any code that I've used off of someone else's AU3 script, I've given credit within the
comments of gwhelper.au3.

Likewise, if you recognize code that I've not given credit to please let me know and I will add it.

Additionally, if I've used code that you've written and you wish it to be remove please notify me and I will.


---------------
III. Known Bugs
---------------
1. Some of the prepackaged bots have functional issues. Will fix later.

------------------------------------
IV. Reporting problems/bugs/glitches
------------------------------------
1. Go into gwhelper.ini and add gwhLog=TRUE under the section [Settings_YourCharacterName]

2. Run GW Helper and do whatever it was you were doing until the problem occurs.

3. Stop GW Helper and post a copy of the gwh.log file along with a description of the problem.

----------------------
V. End-User bot adding
----------------------
This is a summary of section VI. For extensive documentation, see section VI.

To add a bot to GW Helper:

1.	Move the .au3 script to gwhBotScripts folder.

2.	Edit the script and put _ArrayAdd($loadedBotFuncs,"BotFunctionName") at the top of the script. 
	Where "BotFunctionName" is it needs to be the name of the primary function that is called within the bot.
	If there isn't one, you need to make one and move the primary section of the bot there.

3.	Add it to the "include" section of gwhBots.au3

4.	(OPTIONAL) If you want a specific description to come up in GW Helper's bot list, then make
	BotFunctionName.ini in gwhBotInfo folder then create a section called [gwh] and make a key called
	Description and set the value to whatever name you want. Use the other INI files for reference if you're
	still unsure.

5.	Compile gwhelper.au3

6.	Run gwhelper.exe


-------------------------------
VI. Adding/Writing your own bot
-------------------------------
Bots are comprised of three files: The .au3 script, a .ini file, and a .txt file. 

gwhBotScripts/*.au3			The botting script itself.
 
gwhBotInfo/yourbotfunctionname.ini	The settings for your bot.

gwhBotInfo/yourbotfunctionname.txt	The description file

No functionalities (Auto-Looter, Auto-sell, etc..) of GW Helper are automatically called when you run your 
bot. You must manually call those functions yourself.

1.	Make a new .au3 file inside gwhBotScripts folder and add it to the "include" section of gwhBots.au3

2.	Make a primary function that will be called.

3.	Put _ArrayAdd($loadedBotFuncs,"YourBotFunctionName") at the top of your script.

4.	Compile gwhelper.au3

3.	(OPTIONAL) Create and edit your bot's ini/txt file in the gwhBotInfo folder.

4.	Run GW Helper.

5. 	Start your bot.

		--------------
		BD.i Debugging
		--------------
		If you need to debug your bot, set parameters in SCiTE for gwhelper.au3 as listed in Section I.
		You can also set gwhLog=True in the gwhelper.ini file under [Settings_YourCharactersName]and use 
		the logGWH() function to output to gwh.log.

		------------------------------
		BD.ii Adding your own settings
		------------------------------
		If you want to add your own settings for the user to adjust, create botfunctionname.ini in
		gwhBotInfo folder and create a [Settings] section. Add whatever settings you want the user to be 
		able to adjust here. You can access it via iniRead/Write within your bot script using $iniBotFile
		for the file name.

		-------------------------------
		BD.iii Usable functions globals
		-------------------------------
		All functions and globals within GW Helper are usable to you.

		Below is a list of the most relevant globals related to bot makers.

		$selfPtr	Pointer to the player. It is automatically grabbed when the bot is started.
				If you use waitForMap() to wait for your maps to load it is updated for you. 
				Otherwise, you have to do it.

		$iniBotFile	INI settings file current bot being used.

		$iniFile	INI settings for the player

		-------------------
		BD.iv Documentation
		-------------------
		You can add documentation to appear in GW Helper by creating botfunctioname.txt in the gwhBotInfo 
		folder and type everything the user needs to know. After, make a section in your botfunctionname.ini 
		called [gwh] and add key called "Description" then set the value to whatever you want the name of the 
		bot to appear as in GW Helper.


-----------
1. Botting
-----------
1.	Click the bot you want from the bot list.

2.	Adjust the settings if there are any.

3.	Start bot.

4.	End the bot by running GW Helper again
	Alternatively, you can look for gwhelper.exe running in the process list of the task manager and kill it
	yourself.

-----------
2. Autoloot
-----------
Set the radius that you would like it to check for items and toggle it on.

Enemy to radius is the radius around the item it checks for enemies.
If there are any enemies within this radius then it will not loot.

Enemy to player checks if there's any enemies with the radius.
If there are any enemies within this radius then it will not loot.


-------------
3. Nameplates
-------------
Turns on and off all nameplates.


----------
4. Auto-ID
----------
Press HOME to start identifying all items in your bag.

If ID White Items is checked It will identify any non-white item that sells for above the specified price.

------------
5. Hero Sets
------------
Hero Sets are custom per character.

Add a heroset name or select a previous one.

Add heroes in your party in-game that you would like to make up the set and organize them how 
you'd like them to be loaded every time.

Click "Update Heroes".

If you would like to to load a specific template for a hero, click the hero and then click "Set template".
Browse for the template you want it to load and press ok.

Load the heroes into your party.

------------
6. Auto-Sell
------------
Auto-Selling is limited to your starter bag, your belt pouch, and the first bag slot. Your Second bag slot and 
your equipment pouch will not auto-sell.

To Auto-Sell you just open a merchant window and press the INSERT key.

Auto-Sell blacklists these item types: Rune/Mod, Usable, Dye, Materials & Z-Coins, Key, Quest Item, Kit, Trophy, 
Scroll, Books, Costume Headpiece


---------------
7. Auto-Salvage
---------------
Auto-Salvaging is limited to your starter bag, your belt pouch, and the first bag slot. Your equipment pouch and
your second bag (the one to the left of the equipment pouch) will not auto-salvage.

To auto-salvage first go to the auto-salvage tab and add/remove items you would like to be automatically salvaged 
from your items. Then Just press the END key.

You must have a salvage kit in your inventory and the items must be identified already. Consider using auto-id 
before auto-salvage.

As far as I'm know, Auto-Salvage should only salvage perfect values of those items.