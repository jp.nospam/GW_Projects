#NoTrayIcon
#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_Description=GW Helper
#AutoIt3Wrapper_Res_Fileversion=0.0.0.7
#AutoIt3Wrapper_Res_LegalCopyright=2015 GWPlayer
#AutoIt3Wrapper_Run_AU3Check=n
#AutoIt3Wrapper_Tidy_Stop_OnError=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs
    Author: 	jp.nospam

    Script Function:
        A Guild Wars 2 helper/bot

    To-Do:
        --  Add Whitelist/blacklist for autoloot.

		--	Add auto-sell GUI controls.

        --  Fix my bad coding of unncessarily hardcoding global variables into
			functions instead of making them modify/return the values.

        --  Find elegant solution to botLoad loop.

    Change Log:
		0.0.0.7		PoE bot restructuring and bugfixes. Seems to be running
					perfectly now.

					Fixed autosell bug that kept last slot from selling.

		0.0.0.6		Fixed auto-salvage from trying to salvage runes.

					Updated some modstruct finger prints for auto-salvage.

		0.0.0.5		Bot initialization functionality changed.

					killBot updated to work with new bot functionality.

					Kilroy bot added.

					Autolooter improvement/fixes.

					idAll function modified.

					added autoSalvaging function.

					added autoSelling function.

					Added hero list GUI enable/disable checks.

					Added exit on map load loop incase of crash.

					Added zei rei to hero list.
					Thanks to Ralle1976 @ gamerevision.com

					Removed event system & auto interrupter due to functional
					issues with Win 8+.

					Restructured how map loading is handled.
					Still uses waitForMap().

		0.0.0.4		Executes new instances of itself for bots to makeup for
					Autoit's inability to MultiThread.

					Removed adlib KillAll in favor of Main() killall.

					Fixed bug where scrollbar didn't show on heroset
					comboboxes.

					Added new bots: Necro/Rit/Rit2/Ele Elite tome farmer.

					Split bots into separate file for easy and clean
					modification.

					Added debug INI setting.

					Moved modified functions to own au3.
					You can now use your own gwa2 file.

					getGWHSettings and syncGUI properly split.

					Autoloot no longer tries to pickup bundles.

        0.0.0.3 	Added debugging function.

                    Fixed interrupt infinite loop.

					Added GUI support for AutoLoot, AutoInterrupt, Auto-ID.

					Added GUI support for managing HeroSets.

					HeroSets are now based per character.

        0.0.0.2     Support for mercenary heroes added.
	----------------------------------------------------------------------------
#ce

; Custom includes
#include <Misc.au3>
#include <Array.au3>
#include "Gwa2.au3"
#include "gwa2Extend.au3"
#include "gwhGlobals.au3"
#include "gwhBots.au3"

; Exit if no process of guild wars exists
IF NOT processexists("gw.exe") THEN EXIT

logGWH("----------STARTING NEW LOG----------")

; Spawn bot instance if called
IF $CmdLine[0] > 1 THEN
	DO
		$botting = initialize(int($cmdLine[1]), FALSE)
		Sleep(1000)
	UNTIL $botting = TRUE
	getGWHSettings(FALSE)
	Call($CmdLine[2])
	EXIT
ENDIF

; Kill last known running bot
botKill()

; GUI Design
#include <ButtonConstants.au3>
#include <GuiComboBox.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <MsgBoxConstants.au3>
#include <ComboConstants.au3>
#include <GUIListBox.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <StringConstants.au3>

; Use GUI event mode
Opt("GUIOnEventMode", 1)

;==========BEGIN GUI CREATION==========
$gwhForm = GUICreate("GW Helper", 455, 503, 196, 164)
GUISetOnEvent($GUI_EVENT_CLOSE, "gwhFormClose")
GUISetOnEvent($GUI_EVENT_MINIMIZE, "gwhFormMinimize")
GUISetOnEvent($GUI_EVENT_MAXIMIZE, "gwhFormMaximize")
GUISetOnEvent($GUI_EVENT_RESTORE, "gwhFormRestore")
$startbotButton = GUICtrlCreateButton("Start Bot", 16, 176, 75, 25)
GUICtrlSetOnEvent(-1, "startbotButtonClick")
$toggleButton = GUICtrlCreateButton("Toggle", 152, 176, 100, 25)
GUICtrlSetOnEvent(-1, "toggleButtonClick")
$botList = GUICtrlCreateList("", 0, 8, 113, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "")
$toggleList = GUICtrlCreateList("", 144, 8, 121, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "Auto-Loot (OFF)|Name Plates (OFF)")
$utilList = GUICtrlCreateList("", 288, 8, 121, 162, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "Auto-ID [HOME]|Auto-Salvage [END]|Auto-Sell [INSERT]|Call Target [`]")
$generalTab = GUICtrlCreateTab(8, 232, 433, 257)
$generalSheet = GUICtrlCreateTabItem("General")
GUICtrlSetState(-1,$GUI_SHOW)
$luxRedeemCombo = GUICtrlCreateCombo("", 136, 273, 145, 25, BitOR($GUI_SS_DEFAULT_COMBO,$CBS_SIMPLE))
GUICtrlSetData(-1, "Kill Bot|Jadeite Shards|Scrolls Of The Deep|Alliance Reputation")
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetTip(-1, "Wat do when max Luxon?")
GUICtrlSetOnEvent(-1, "luxRedeemComboChange")
$Label2 = GUICtrlCreateLabel("Luxon Redeem Method", 16, 273, 117, 17)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$heroSetsSheet = GUICtrlCreateTabItem("Hero Sets")
$heroesList = GUICtrlCreateList("", 150, 289, 121, 160, BitOR($WS_BORDER, $WS_VSCROLL))
GUICtrlSetData(-1, "")
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$heroSetLabel = GUICtrlCreateLabel("Hero Set", 40, 265, 64, 25)
GUICtrlSetFont(-1, 12, 400, 0, "Segoe UI")
$heroesLabel = GUICtrlCreateLabel("Heroes", 184, 265, 53, 25)
GUICtrlSetFont(-1, 12, 400, 0, "Segoe UI")
$loadHeroesButton = GUICtrlCreateButton("Load Heros", 152, 457, 115, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "loadHeroesButtonClick")
$addHeroCombo = GUICtrlCreateCombo("", 280, 296, 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$newHeroSetButton = GUICtrlCreateButton("Add New Set", 24, 328, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "newHeroSetButtonClick")
$addHeroButton = GUICtrlCreateButton("Add Hero", 280, 328, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "addHeroButtonClick")
$removeHeroButton = GUICtrlCreateButton("Remove Hero", 280, 360, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "removeHeroButtonClick")
$heroSetCombo = GUICtrlCreateCombo("", 16, 296, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "heroSetComboChange")
$removeHeroSet = GUICtrlCreateButton("Remove Set", 24, 360, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "removeHeroSetClick")
$autolootSheet = GUICtrlCreateTabItem("Auto-Loot")
$lootDistanceInput = GUICtrlCreateInput("", 152, 264, 73, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label3 = GUICtrlCreateLabel("Radius:", 96, 264, 40, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label4 = GUICtrlCreateLabel("Enemy To Item Radius:", 32, 296, 112, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$enemyItemRadiusInput = GUICtrlCreateInput("", 152, 296, 121, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$enemyPlayerRadiusInput = GUICtrlCreateInput("", 152, 328, 121, 22, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$Label5 = GUICtrlCreateLabel("Enemy To Player Radius:", 24, 328, 123, 18)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$autoIDSheet = GUICtrlCreateTabItem("Auto-ID")
$idValueInput = GUICtrlCreateInput("", 144, 264, 121, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
$idWhiteCheckbox = GUICtrlCreateCheckbox("ID White Items", 144, 288, 97, 17)
$Label6 = GUICtrlCreateLabel("Minimum White ID Value:", 16, 264, 123, 17)
$saveAIDButton = GUICtrlCreateButton("Save", 160, 312, 75, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetOnEvent(-1, "saveAIDButtonClick")
$autoSalvageSheet = GUICtrlCreateTabItem("Auto-Salvage")
$prefixListbox = GUICtrlCreateList("", 24, 320, 121, 123)
$addPrefixButton = GUICtrlCreateButton("Add Prefix", 24, 303, 121, 17)
GUICtrlSetOnEvent(-1, "addPrefixButtonClick")
$removePrefixButton = GUICtrlCreateButton("Remove Prefix", 24, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removePrefixButtonClick")
$prefixCombo = GUICtrlCreateCombo("", 24, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$addInscriptionButton = GUICtrlCreateButton("Add Inscription", 160, 304, 121, 17)
GUICtrlSetOnEvent(-1, "addInscriptionButtonClick")
$inscriptionCombo = GUICtrlCreateCombo("", 160, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$inscriptionListbox = GUICtrlCreateList("", 160, 320, 121, 123)
$removeInscriptionButton = GUICtrlCreateButton("Remove Inscription", 160, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removeInscriptionButtonClick")
$addSuffixButton = GUICtrlCreateButton("Add Suffix", 296, 304, 121, 17)
GUICtrlSetOnEvent(-1, "addSuffixButtonClick")
$suffixCombo = GUICtrlCreateCombo("", 296, 272, 121, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL,$WS_VSCROLL))
$suffixListbox = GUICtrlCreateList("", 296, 320, 121, 123)
$removeSuffixButton = GUICtrlCreateButton("Remove Suffix", 296, 448, 120, 25)
GUICtrlSetOnEvent(-1, "removeSuffixButtonClick")
GUICtrlCreateTabItem("")
$Label1 = GUICtrlCreateLabel("Utilities", 328, 184, 37, 17)
GUISetState(@SW_SHOW)
;==========END GUI CREATION==========

; Load settings into globals and syncGUI.
getGWHSettings()

#cs
    Loop to do nothing until bot loads into Guild Wars because
    I'm too lazy to add a GUI to it at the moment.

    Considering the gwa2 toolbox solution to this.
#ce
logGWH("Attemping to inject GWA2...")
DO
    $botLoaded = loadBot()
     sleep(1000)
 UNTIL $botLoaded = TRUE
logGWH("GWA2 injection complete.")

;==========BEGIN CORE FUNCTIONS==========

#cs
    Function:       logGWH

    Parameters:
        $text       Text to write to the log file.

        $fName      Log file name.

    Return:
        None

    Description:    Output text to a log file.
#ce
FUNC logGWH($text = "Unknown log output.", $fName = "gwh.log")
    IF $GWH_LOG = "False" THEN RETURN
    $fHwnd = FileOpen($fName, 1)
    FileWriteLine($fHwnd, @HOUR & ":" & @MIN & @TAB & $text)
    FileClose($fHwnd)
ENDFUNC

#cs
    Function:       getGWHSettings

    Parameters:
        $sync		TRUE/FALSE. Decide whether to sync the gui with
					the settings.

    Return:
        None

    Description:    Read INI settings and assigns them to variables.
#ce
FUNC getGWHSettings($sync = TRUE)
	$currentChar = getCharName()
	local $iniSalvageSettings = inireadsection($iniFile, "Autosalvage")
    $iniSettings = inireadsection($iniFile, "settings")
	$iniBotList = inireadsection($iniFile, "BotList")
    $iniHeroSet = inireadsection($iniFile, "HeroSet_" & $currentChar)

	FOR $x = 1 TO $iniSettings[0][0]
        SWITCH $iniSettings[$x][0]
        CASE "autoloot"
            IF $iniSettings[$x][1] == "True" THEN
                $autoloot = TRUE
            ELSE
                $autoloot = FALSE
			ENDIF
        CASE "namePlates"
            IF $iniSettings[$x][1] == "True" THEN
                $nameToggle = TRUE
            ELSE
                $nameToggle = FALSE
            ENDIF
		CASE "autoIDWhite"
            IF $iniSettings[$x][1] == "True" THEN
                $idWhite = TRUE
            ELSE
                $idWhite = FALSE
            ENDIF
		CASE "autoIDVal"
			$minIDVal = $iniSettings[$x][1]
        CASE "luxRedeem"
            $luxRedeem = $iniSettings[$x][1]
        CASE "autolootdistance"
            $autolootdistance = $iniSettings[$x][1]
		CASE "enemyToItem"
            $enemyItemRadius = $iniSettings[$x][1]
		CASE "enemyToPlayer"
            $enemyPlayerRadius = $iniSettings[$x][1]
        CASE "skillFolder"
            $skillFolder = $iniSettings[$x][1]
        CASE "interruptList"
            $interruptList = stringsplit($iniSettings[$x][1], ",")
        CASE "interrupt"
            IF $iniSettings[$x][1] == "True" THEN
                $interruptToggle = TRUE
            ELSE
                $interruptToggle = FALSE
            ENDIF
        ENDSWITCH
	NEXT

	For $x = 1 To $iniSalvageSettings[0][0] - 1
		switch $iniSalvageSettings[$x][0]
			case "mod1"
				$iniSalvageMod1 = stringsplit($iniSalvageSettings[$x][1], ",")
			Case "mod2"
				$iniSalvageMod2 = stringsplit($iniSalvageSettings[$x][1], ",")
			case "inscription"
				$iniSalvageInscr = stringsplit($iniSalvageSettings[$x][1], ",")
			case "salvageables"
				if $iniSalvageSettings[$x][1] == "True" THEN $iniSalvagables = True
				if $iniSalvageSettings[$x][1] == "False" THEN $iniSalvagables = False
		endswitch

	Next

    IF $sync == TRUE THEN syncGUI()
ENDFUNC

#cs
    Function:       loadBot

    Parameters:
		None

    Return:
        True        Successful injection.

        False       Unsucessful injection.

    Description:    Tries to load GWA2 into Guild Wars Title

    Notes:          Merge this crap into syncGUI.
#ce
FUNC loadBot()
    IF $initHwnd = FALSE THEN $initHwnd = WingetProcess("Guild Wars")
    IF initialize($initHwnd, FALSE) THEN
		$currentChar = getCharName()
        WinSetTitle($gwhForm, "", "GW Helper - " & $currentChar)
        DisplayAll($nameToggle)
		getGWHSettings()
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

#cs
    Function:       syncGUI

    Parameters:
        None

    Return:
        None

    Description:    Updates GUI from settings.
#ce
FUNC syncGUI()
    logGWH("Syncronizing GUI...")

	GuiCtrlSetState($addHeroButton,$GUI_DISABLE)
	GuiCtrlSetState($removeHeroButton,$GUI_DISABLE)
	GuiCtrlSetState($removeHeroSet,$GUI_DISABLE)
	GuiCtrlSetState($loadHeroesButton,$GUI_DISABLE)


	IF $autoloot == TRUE THEN
		_GUICtrlListBox_ReplaceString($toggleList,0,"Auto-Loot (ON)")
		guictrlsetstate($lootDistanceInput, $GUI_DISABLE)
		guictrlsetstate($enemyItemRadiusInput, $GUI_DISABLE)
		guictrlsetstate($enemyPlayerRadiusInput, $GUI_DISABLE)
	ELSE
		_GUICtrlListBox_ReplaceString($toggleList,0,"Auto-Loot (OFF)")
		guictrlsetstate($lootDistanceInput, $GUI_ENABLE)
		guictrlsetstate($enemyItemRadiusInput, $GUI_ENABLE)
		guictrlsetstate($enemyPlayerRadiusInput, $GUI_ENABLE)
	ENDIF


	IF $nameToggle == TRUE THEN
		_GUICtrlListBox_ReplaceString($toggleList,1,"Name Plates (ON)")
	ELSE
		_GUICtrlListBox_ReplaceString($toggleList,1,"Name Plates (OFF)")
	ENDIF

	IF $idWhite == TRUE THEN
		GUICtrlSetState($idWhiteCheckbox, 1)
	ELSE
		GUICtrlSetState($idWhiteCheckbox, 4)
	ENDIF


	GuiCtrlSetData($idValueInput, $minIdVal)

	_GUICtrlComboBox_SetCurSel($luxRedeemCombo, $luxRedeem)

	guictrlsetdata($lootDistanceInput, $autolootdistance)

	guictrlsetdata($enemyItemRadiusInput, $enemyItemRadius)

	guictrlsetdata($enemyPlayerRadiusInput, $enemyPlayerRadius)

	GUICtrlSetData($heroSetCombo, "")
	IF IsArray($iniHeroSet) Then
		FOR $x = 1 TO $iniHeroSet[0][0]
			_GUICtrlComboBox_AddString($heroSetCombo, $iniHeroSet[$x][0])
		NEXT
	ENDIF

	GUICtrlSetData($botList, "")
	IF IsArray($iniBotList) Then
		FOR $x = 1 TO $iniBotList[0][0]
			_GUICtrlListBox_AddString($botList, $iniBotList[$x][1])
		NEXT
	ENDIF

	GuiCtrlSetData($prefixCombo, "")
	GuiCtrlSetData($suffixCombo, "")
	GuiCtrlSetData($inscriptionCombo, "")
	GuiCtrlSetData($suffixListbox, "")
	GUICtrlSetData($prefixListbox, "")
	guictrlsetdata($inscriptionListbox, "")

	; Populate prefixes
	For $p = 0 TO UBound($prefixList) - 1
		if _ArraySearch($iniSalvageMod1, $prefixList[$p][0]) == -1 Then
			_GUICtrlComboBox_AddString($prefixCombo,$prefixList[$p][0])
		ELSE
			_GUICtrlListBox_AddString($prefixListbox,$prefixList[$p][0])
		ENDIF
	NEXT

	if _GUICtrlComboBox_GetCount($prefixCombo) > 0 THEN
		_GUICtrlComboBox_SetCurSel($prefixCombo,0)
		GUIctrlsetstate($addPrefixButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($addPrefixButton, $GUI_DISABLE)
	endif

	if _GUICtrlListBox_GetCount($prefixListbox) > 0 THEN
		GUIctrlsetstate($removePrefixButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($removePrefixButton, $GUI_DISABLE)
	endif

	; Populate inscriptions
	For $p = 0 TO UBound($inscrList) - 1
		if _ArraySearch($iniSalvageInscr, $inscrList[$p][0]) == -1 Then
			_GUICtrlComboBox_AddString($inscriptionCombo,$inscrList[$p][0])
		ELSE
			_GUICtrlListBox_AddString($inscriptionListbox,$inscrList[$p][0])
		ENDIF
	NEXT

	if _GUICtrlComboBox_GetCount($inscriptionCombo) > 0 THEN
		_GUICtrlComboBox_SetCurSel($inscriptionCombo,0)
		GUIctrlsetstate($addInscriptionButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($addInscriptionButton, $GUI_DISABLE)
	endif

	if _GUICtrlListBox_GetCount($inscriptionListbox) > 0 THEN
		GUIctrlsetstate($removeInscriptionButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($removeInscriptionButton, $GUI_DISABLE)
	endif

	; Populate suffixes
	For $p = 0 TO UBound($suffixList) - 1
		if _ArraySearch($iniSalvageMod2, $suffixList[$p][0]) == -1 Then
			_GUICtrlComboBox_AddString($suffixCombo,$suffixList[$p][0])
		ELSE
			_GUICtrlListBox_AddString($suffixListbox,$suffixList[$p][0])
		ENDIF
	NEXT

	if _GUICtrlComboBox_GetCount($suffixCombo) > 0 THEN
		_GUICtrlComboBox_SetCurSel($suffixCombo,0)
		GUIctrlsetstate($addSuffixButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($addSuffixButton, $GUI_DISABLE)
	endif

	if _GUICtrlListBox_GetCount($suffixListbox) > 0 THEN
		GUIctrlsetstate($removeSuffixButton, $GUI_ENABLE)
	else
		GUIctrlsetstate($removeSuffixButton, $GUI_DISABLE)
	endif

	logGWH("GUI sync done.")
ENDFUNC

#cs
    Function:       addHeroes

    Parameters:
        $hList      List of hero id's to add. Delimited by commas.
                    Uses $hList[0] as the max index of array.

        $template   To be implemented with loadHeroTemplates.

    Return:
        None

    Description:    Adds list of heroes to your party.
#ce
FUNC addHeroes($hList = FALSE, $template = FALSE)
    IF $hList == FALSE THEN RETURN

    $hList = stringsplit($hList, ",")
    logGWH("Adding heroes to party...")
    FOR $x = 1 TO $hList[0]
        $hID = _ArraySearch($HEROES, $hList[$x])
        $hNum = GetHeroNumberByHeroId($hID)
        IF $hID > -1 AND $hNum == 0 THEN
			AddHero($hID)
        ENDIF
    NEXT
    logGWH("Hero party addition complete.")
ENDFUNC

#cs
    Function:       loadHeroTemplates

    Parameters:
        $tGroup     Array of template to hero list.
                    Array is made from IniR.adSection.

    Return:
        None

    Description:    Loads a predefined template list onto your heros.

    Notes:          CURRENTLY UNUSABLE. See: loadHeroesButtonClick()
#ce
FUNC loadHeroTemplates($tGroup = FALSE)
    IF $tGroup == FALSE THEN RETURN

    $hNum = 0

    logGWH("Adjusting hero templates...")
    FOR $x = 1 TO $tGroup[0][0]
        $hID = _ArraySearch($HEROES, $tGroup[$x][0])

        $hNum = GetHeroNumberByHeroId($hID)
        IF $hNum == 0 THEN RETURN

        $fPath = $skillFolder & $tGroup[$x][1]
        IF fileexists($fPath) == 0 THEN CONTINUELOOP

        $fileHwnd = FileOpen($fPath)
        $tCode = FileReadLine($fileHwnd, 1)
        FileClose($fileHwnd)
        LoadSkillTemplate($tCode, $hNum)
    NEXT
    logGWH("Hero template adjustment complete.")
ENDFUNC

#cs
    Function:       waitForMap

    Parameters:
		None

    Return:
        None

    Description:	Pauses script waiting for map to load.

	Notes:			Relies on the GWA2 event system to set the variable to false.
#ce
FUNC waitForMap()
	logGWH("Waiting for load state...")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetMapLoading() == 2

	logGWH("Waiting for map to load.")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetMapLoading() <> 2
	logGWH("Map loaded.")

	logGWH("Waiting for agent to load.")
	DO
		IF NOT ProcessExists("gw.exe") THEN EXIT
		Sleep(100)
	UNTIL GetAgentExists(-2)
	logGWH("Agent loaded.")

    IF $nameToggle == TRUE THEN DisplayAll($nameToggle)

	IF $CmdLine[0] > 1 THEN RETURN

	logGWH("Getting character name.")
	IF $currentChar <> getCharName() THEN
		$currentChar = getCharName()
		getGWHSettings()
		WinsetTitle($gwhForm, "", "GW Helper - " & $currentChar)
	ENDIF
ENDFUNC

#cs
    Function:       botKill

    Parameters:
		None

    Return:
		None
#ce
FUNC botKill()
	$killBot = iniread($iniFile, "Settings", "botPID", "")
	IF int($killBot) <= 0 OR NOT ProcessExists($killBot) THEN
		iniwrite($iniFile, "Settings", "botPID", "")
		RETURN
	ENDIF
	ProcessClose($killBot)
	iniwrite($iniFile, "Settings", "botPID", "")
ENDFUNC
;==========END CORE FUNCTIONS==========

#cs
    Function:       idAll

    Parameters:
		$idWhite	Boolean value that determines if it should identify white items

		$idVal		INT value to determine at what price to identify white items

    Return:
        None

    Description:    ID's all items in every bag.
#ce
FUNC idAll($idWhite = FALSE, $whiteVal = 30)
	WriteChat("ID starting...")
	ext_IdentifyBag(1, $idWhite, $whiteVal)
	ext_IdentifyBag(2, $idWhite, $whiteVal)
	ext_IdentifyBag(3, $idWhite, $whiteVal)
	ext_IdentifyBag(4, $idWhite, $whiteVal)
	ext_IdentifyBag(5, $idWhite, $whiteVal)
	if FindIDKit() == 0 THEN
		WriteChat("Ran out of ID kits. ID incomplete.")
		return 0
	ELSE
		WriteChat("ID complete.")
		return 1
	ENDIF
ENDFUNC

#cs
    Function:       sellAll

    Parameters:
		None

    Return:
        None

    Description:    Sells items in first 3 bags.
#ce
FUNC sellAll()
	WriteChat("Selling...")
	sellBag(1)
	sellBag(2)
	sellBag(3)
	WriteChat("Sell complete.")
ENDFUNC

#cs
    Function:       	sellCheck

    Parameters:
		$iObj			Item object to check.

    Return:
        None

    Description:   		Checks items to sell.
#ce
Func sellCheck($iObj)
	local $noSellType[] = [8,9,10,11,18,21,29,30,31,43,45]

	$iType = DllStructGetData($iObj, "type")

	for $x = 0 to ubound($noSellType) - 1
		if $noSellType[$x] == int($iType) THEN RETURN FALSE
	Next
	RETURN TRUE
ENDFUNC

#cs
    Function:       	sellBag

    Parameters:
		$bagIndex		Index of bag to sell.

    Return:
        None

    Description:    	Sell items in a bag.

	Credits:			@gamerevision.com: anon777 (creator), mastor (update), kjohn9815 (fix)
#ce
Func sellBag($bagIndex)
	$bag = GetBag($bagIndex)
	$numOfSlots = DllStructGetData($bag, 'slots')
	For $i = 1 To $numOfSlots
		$item = GetItemBySlot($bagIndex, $i)
		if dllstructgetdata($item, "id") == 0 THEN ContinueLoop
		If sellCheck($item) == True Then
			SellItem($item)
			Sleep(250)
		EndIf
	Next
EndFunc

#cs
    Function:       	salvageAll

    Parameters:
		$salv			Decides whether to salvage salvageable items.

    Return:
        None

    Description:    	Salvage all items in first 3 bags.
#ce
FUNC salvageAll($salv = FALSE)
	WriteChat("Salvaging...")
	salvageBag(1, $salv)
	salvageBag(2, $salv)
	salvageBag(3, $salv)
	WriteChat("Salvage complete.")
ENDFUNC

#cs
    Function:       	salvageBag

    Parameters:
		$bagIndex		Index of bag to salvage.

		$salvagables	Decides whether to salvage salvageable items.

    Return:
        None

    Description:    	Salvage all items in a bag.
#ce
FUNC salvageBag($bagIndex, $salvagables = FALSE)
	$bag = GetBag($bagIndex)
	$numOfSlots = DllStructGetData($bag, 'slots') - 1

	For $i = 0 To $numOfSlots
		$item = GetItemBySlot($bagIndex, $i)
		if dllstructgetdata($item, "id") == 0 OR dllstructgetdata($item, "Type") == 8 THEN ContinueLoop

		For $x = 3 To 0 Step - 1
			$sCheck = salvageCheck($item, $salvagables, $x)
			If $sCheck > -1 Then
				StartSalvage($item)
				sleep(500)
				if $sCheck == 3 THEN
					SalvageMaterials()
				ELSE
					SalvageMod($sCheck)
				Endif
				Sleep(250)
			EndIf
		Next
	Next

ENDFUNC

#cs
    Function:       	salvageCheck

    Parameters:
		$iObj			Item object/struct

		$salvagables	Decides whether to salvage salvageable items.

		$modCheck		Which modification to check to see if it should be salvaged.
						Refer to return values to adjust this parameter to match the
						type of check you want.

    Return:
		-1				Item should not be salvaged.

		0				Item prefix should be salvaged

		1				Item suffix should be salvaged.

		2				Item inscription should be salvaged.

        3				Item is a slavageable and should be salvaged.

    Description:    	Checks to see if an item's mod should be salvaged.
#ce
FUNC salvageCheck($iObj, $salvagables = FALSE, $modCheck = 3)
	If  DllStructGetData($iObj, "type") == 0 AND $salvagables == TRUE AND $modCheck == 3 THEN RETURN 3

	$modStruct = string(GetModStruct($iObj))

	if $iniSalvageMod2[0] >= 1 AND $modCheck == 1 AND $iniSalvageMod2[1] <> "" Then
		For $m = 1 TO $iniSalvageMod2[0]
			$val = _ArraySearch($suffixList, $iniSalvageMod2[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $suffixList[$val][1]) <> 0 THEN RETURN 1
			endif
		Next
	ENDIF

	if $iniSalvageMod1[0] >= 1 AND $modCheck == 0 AND $iniSalvageMod1[1] <> "" Then
		For $m = 1 TO $iniSalvageMod1[0]
			$val = _ArraySearch($prefixList, $iniSalvageMod1[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $prefixList[$val][1]) <> 0 THEN RETURN 0
			endif
		Next
	ENDIF

	if $iniSalvageInscr[0] >= 1 AND $modCheck == 2 AND $iniSalvageInscr[1] <> "" Then
		For $m = 1 TO $iniSalvageInscr[0]
			$val = _ArraySearch($inscrList, $iniSalvageInscr[$m], 0 , 0, 0 , 0, 1, 0)
			if $val > -1 THEN
				if StringInStr($modStruct, $inscrList[$val][1]) <> 0 THEN RETURN 2
			endif
		Next
	ENDIF

	RETURN -1
ENDFUNC

#cs
    Function:       toggleNamePlates

    Parameters:
        None

    Return:
        None

    Description:    Toggles the nameplates' true/false variable.
#ce
FUNC toggleNamePlates()
    IF $nameToggle == TRUE THEN
        $nameToggle = FALSE
    ELSE
        $nameToggle = TRUE
    ENDIF

    iniwrite($iniFile, "Settings", "namePlates", $nameToggle)

    DisplayAll($nameToggle)
ENDFUNC

#cs
    Function:			lootItems

    Parameters:
        $itemToPlayer   Max radius it will search for dropped items.

        $itemToEnemy    Radius to search for enemies around the item.
                        Will NOT loot if an enemy is within radius to item.

        $enemyToPlayer  Radius to search for enemies around the player.
                        Will NOT loot if an enemy is within radius to the player.

    Return:
        0				Unable to pickup item because game/player is busy.

		1				No items to pickup.

		2				Item out of loot radius.

		3				Enemy near item or player.

		4				Attempting to pick up item.

    Description:		Automatically loots dropped items.
#ce
FUNC lootItems($itemToPlayer = 850, $itemToEnemy = 850 ,$enemyToPlayer = 850)

	IF GetMapLoading() <> 1 OR GetAgentByID(-1) <> 0 OR GetIsMoving(-2) == TRUE OR ext_GetIsCasting(GetAgentByID()) == TRUE THEN RETURN 0

    $tItem = ext_GetNearestItemToPlayer()
    IF $tItem == -1 OR GetIsMovable($tItem) == FALSE OR ext_GetBagSpace() == 0 THEN RETURN 1

    $cDist = getDistance($tItem)
    IF $cDist > $itemToPlayer THEN RETURN 2

    $itemEnemy = GetNearestEnemyToAgent($tItem)
    $selfEnemy = GetNearestEnemyToAgent()

    if getDistance($tItem, $itemEnemy) < $itemToEnemy OR getDistance($selfEnemy, -2) < $enemyToPlayer  THEN RETURN 3

    Move(DllStructGetData($tItem, 'X'), DllStructGetData($tItem, 'Y'))

    IF $cDist < 50 THEN
        PickUpItem($tItem)
        sleep(800)
    ENDIF

	RETURN 4
ENDFUNC

#cs
    Function:       toggleAutoLoot

    Parameters:
        None

    Return:
        None

    Description:    Toggles the autoloot's true/false variable.
#ce
FUNC toggleAutoLoot()
    IF $autoloot == TRUE THEN
        $autoloot = FALSE
    ELSE
        $autoloot = TRUE
        iniwrite($iniFile, "Settings", "autolootdistance", GUICtrlRead($lootDistanceInput))
		iniwrite($iniFile, "Settings", "enemyToItem", GUICtrlRead($enemyItemRadiusInput))
		iniwrite($iniFile, "Settings", "enemyToPlayer", GUICtrlRead($enemyPlayerRadiusInput))
    ENDIF

    iniwrite($iniFile, "Settings", "autoloot", $autoloot)
ENDFUNC

#cs
    Function:       getItemModStructDiff

    Parameters:
        None

    Return:
        None

    Description:    Gets the mod struct difference between
					bag 1 slot 1 (plain) and bag 1 slot 2 (modded) items.
					Use in PvP to get struct values for salvager.
#ce
FUNC getItemModStructDiff()
	$item1 = getitembyslot(1,1)
	$item2 = getitembyslot(1,2)

	$s1 = string(GetModStruct($item1))
	$s2 = string(GetModStruct($item2))

	$mName = inputbox("Name?", "Name?")

	$mFwnd = fileopen("modstruct.txt", 1)
	FileWrite($mFwnd, $mName & " : ")
	For $x = 1 To StringLen($s1)
		If StringMid($s1,$x,1) <> StringMid($s2,$x,1) Then
			$rTrim = StringLen($s1) - $x
			$s2 = stringtrimleft($s2, $x + 7)
			$s2 = stringtrimright($s2, $rTrim + 1)
			FileWrite($mFwnd, $s2 & @CRLF & @CRLF)
			exitloop
		EndIf
	Next
	fileclose($mFwnd)
ENDFUNC

FUNC Main()
	IF NOT processexists("gw.exe") THEN EXIT

	if GetMapLoading() == 2 THEN waitForMap()

	; Autolooter
	IF $autoloot == TRUE THEN lootItems($autolootdistance, $enemyItemRadius, $enemyPlayerRadius)

	; Salvage Bags
	if _IsPressed(23) Then
		WHILE  _IsPressed(23)
			sleep(100)
		WEND
		salvageAll($iniSalvagables)
	ENDIF

	; ID Bags
    IF _IsPressed(24) THEN
		WHILE  _IsPressed(24)
			sleep(100)
		WEND
		idAll($idWhite, $minIDVal)
	ENDIF

	; Sell Bags
	if _IsPressed("2D") Then
		WHILE  _IsPressed("2D")
			sleep(100)
		WEND
		sellAll()
		;getItemModStructDiff()
	ENDIF

	; Call Target
	IF _IsPressed("C0") AND GetAgentByID(-1) <> 0 THEN
		WHILE _IsPressed("C0")
			sleep(100)
		WEND
		CallTarget(GetAgentByID(-1))
	ENDIF
ENDFUNC

WHILE 1
	Main()
    SLEEP(100)
WEND

;==========START GUI EVENT FUNCTIONS==========

Func gwhFormClose()
	Exit
EndFunc

Func gwhFormMaximize()

EndFunc

Func gwhFormMinimize()
EndFunc

Func gwhFormRestore()
EndFunc

FUNC saveAIDButtonClick()
	$tIDVal = GUICtrlRead($idValueInput)
	$tWhite = GuiCtrlRead($idWhiteCheckbox)

	IF $tIDVal < 1 THEN $tIDVal = 0

	IF $tWhite == 1 THEN
		$tWhite = "True"
	ELSE
		$tWhite = "False"
	ENDIF

	IniWrite($iniFile, "Settings", "autoIDWhite", $tWhite)
	IniWrite($iniFile, "Settings", "autoIDval", $tIDVal)
	getGWHSettings()
ENDFUNC

FUNC newHeroSetButtonClick()
	$iBoxHeroSet = inputbox("HeroSet Name?", "Please input a new hero set name.")
	IF $iBoxHeroSet <> "" THEN
		iniwrite($iniFile, "HeroSet_" & $currentChar, $iBoxHeroSet, "")
	ENDIF
	getGWHSettings()
	_GUICtrlComboBox_SetCurSel($herosetCombo, _GUICtrlComboBox_GetCount($heroSetCombo) - 1)
	heroSetComboChange()
ENDFUNC

Func addHeroButtonClick()
	IF _GUICtrlComboBox_GetCurSel($addHeroCombo) < 0 THEN RETURN

	$lCount = _GUICtrlListBox_GetCount($heroesList)
	IF $lCount >= 7 Then
		MsgBox(0, "Full party.", "You party is full. You cannot add more heroes.")
		RETURN
	ENDIF

	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo)
    IF $curSel = -1 THEN RETURN

    $cursel += 1

	$newHeroString = GUICtrlRead($addHeroCombo)


	IF $lCount > 0 THEN $newHeroString = $iniHeroSet[$curSel][1] & "," & GUICtrlRead($addHeroCombo)

	iniwrite($iniFile, "HeroSet_" & $currentChar, GUICtrlRead($heroSetCombo), $newHeroString)
	getGWHSettings()
	_GUICtrlComboBox_SetCurSel($heroSetCombo, $curSel - 1)
	heroSetComboChange()
EndFunc

Func removeHeroButtonClick()
	IF _GUICtrlListBox_GetCurSel($heroesList) == -1 THEN RETURN

	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo) + 1
	$hsList = ""
	$listCount = _GUICtrlListBox_GetCount($heroesList)
	$rHero = GuiCtrlRead($heroesList)

	IF $listCount > 1 Then

		$nList = StringSplit($iniHeroSet[$curSel][1],",")
		$rHero = _ArraySearch($nList, $rHero)
		FOR $x = 1 TO $nList[0]
			IF $x == $rHero THEN CONTINUELOOP
			$hsList = $hsList & $nList[$x] & ","
		NEXT

		$hsList = StringTrimRight($hsList, 1)

	ENDIF

	IniWrite($iniFile, "HeroSet_" & $currentChar, $iniHeroSet[$curSel][0], $hsList)
	getGWHSettings()
	_GUICtrlComboBox_SetCurSel($heroSetCombo, $curSel - 1)
	heroSetComboChange()
EndFunc

Func removeHeroSetClick()
	IniDelete($iniFile, "HeroSet_" & $currentChar, GUICtrlRead($herosetCombo))
	getGWHSettings()
	heroSetComboChange()
	GuiCtrlSetData($heroesList, "")
	GUICtrlSetData($addHeroCombo, "")
EndFunc

Func loadHeroesButtonClick()
    $curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo)

	IF $curSel = -1 THEN RETURN

    addHeroes($iniHeroSet[$curSel + 1][1])

    #cs
    ENABLE WHEN GETATTRIBUTEBYID() IS FIXED

	$iniCat = inireadsection($iniFile, "Template_" & _GUICtrlComboBox_GetText($heroSetCombo, $curSel))

	IF NOT IsArray($iniCat) THEN RETURN
    loadHeroTemplates($iniCat)
    #ce
EndFunc

Func luxRedeemComboChange()
    $luxVal = _GUICtrlComboBox_GetCurSel($luxRedeemCombo)
    iniwrite($iniFile, "Settings", "LuxRedeem", $luxVal)
    $luxRedeem = $luxVal
EndFunc

Func heroSetComboChange()
	$curSel = _GUICtrlComboBox_GetCurSel($heroSetCombo)
	IF $curSel = -1 THEN RETURN

	GuiCtrlSetState($addHeroButton,$GUI_ENABLE)
	GuiCtrlSetState($removeHeroSet,$GUI_ENABLE)
	GuiCtrlSetState($loadHeroesButton,$GUI_ENABLE)

    $cursel += 1
    $hList = stringsplit($iniHeroSet[$curSel][1], ",")
    GUICtrlSetData($heroesList, "")

    logGWH("Populating Heroes List...")
	$added = -1
    FOR $x = 1 TO $hList[0]
		IF $hList[$x] == "" THEN CONTINUELOOP
        $added = _GUICtrlListBox_AddString($heroesList, $hList[$x])
    NEXT
	if $added <> -1 THEN GuiCtrlSetState($removeHeroButton,$GUI_ENABLE)
    logGWH("Heroes list population complete.")

	logGWH("Populating selectable heroes...")
	GUICtrlSetData($addHeroCombo, "")
	FOR $x = 0 TO UBOUND($HEROES) - 1
		IF _GUICtrlListBox_FindInText($heroesList, $HEROES[$x]) > -1 THEN CONTINUELOOP
		_GUICtrlComboBox_AddString($addHeroCombo, $HEROES[$x])
	NEXT
	logGWH("Selectable heroes populated.")
	_GUICtrlComboBox_SetCurSel($addHeroCombo,0)
Endfunc

Func startbotButtonClick()
	$selectedBot = _GUICtrlListBox_GetCurSel($botList)

	$killBot = ShellExecute(@ScriptName, $initHwnd & " " & $iniBotList[$selectedBot + 1][0])
	IniWrite($iniFile, "Settings", "botPID", $killBot)
	Exit
EndFunc

Func toggleButtonClick()
    IF $botLoaded == FALSE THEN RETURN
	$select = 0
    SWITCH _GUICtrlListBox_GetCurSel($toggleList)
    CASE 0
        toggleAutoLoot()
    CASE 1
        toggleNamePlates()
        $select = 1
    ENDSWITCH
	getGWHSettings()
	_GuiCtrlListBox_SetCurSel($toggleList, $select)
EndFunc

func addPrefixButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($prefixCombo)

	IF $curSel < 0 THEN RETURN

	$cPrefixes = _GUICtrlListBox_GetCount($prefixListbox)
	if $cPrefixes < 1 Then
		iniwrite($iniFile, "Autosalvage", "mod1", GuiCtrlRead($prefixCombo))
	Else
		$t = ""
		For $p = 0 TO $cPrefixes - 1
			$t = $t & _GUICtrlListBox_GetText($preFixListBox, $p) & ","
		NEXT
		iniwrite($iniFile, "Autosalvage", "mod1", $t & GuiCtrlRead($prefixCombo))
	ENDIF

	getGWHSettings()
Endfunc

func removePrefixButtonClick()
	$cPrefix = _GUICtrlListBox_GetCurSel($prefixListBox)

	IF $cPrefix < 0 THEN Return

	$tPrefixes = _GUICtrlListBox_GetCount($prefixListBox)
	$nPrefixes = ""
	For $p = 0 TO $tPrefixes - 1
		if $p <> $cPrefix THEN $nPrefixes = $nPrefixes & _GUICtrlListBox_GetText($prefixListBox, $p) & ","
	NExt
	$nPrefixes = StringTrimRight($nPrefixes, 1)

	iniwrite($iniFile, "Autosalvage", "mod1", $nPrefixes)

	getGWHSettings()
Endfunc

func addInscriptionButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($inscriptionCombo)

	IF $curSel < 0 THEN RETURN

	$cInscriptions = _GUICtrlListBox_GetCount($inscriptionListbox)
	if $cInscriptions < 1 Then
		iniwrite($iniFile, "Autosalvage", "inscription", GuiCtrlRead($inscriptionCombo))
	Else
		$t = ""
		For $p = 0 TO $cInscriptions - 1
			$t = $t & _GUICtrlListBox_GetText($inscriptionListbox, $p) & ","
		NEXT
		iniwrite($iniFile, "Autosalvage", "inscription", $t & GuiCtrlRead($inscriptionCombo))
	ENDIF

	getGWHSettings()
Endfunc

func removeInscriptionButtonClick()
	$cInscriptions = _GUICtrlListBox_GetCurSel($inscriptionListBox)

	IF $cInscriptions < 0 THEN Return

	$tInscriptions = _GUICtrlListBox_GetCount($inscriptionListBox)
	$nInscriptions = ""
	For $p = 0 TO $tInscriptions - 1
		if $p <> $cInscriptions THEN $nInscriptions = $nInscriptions & _GUICtrlListBox_GetText($inscriptionListBox, $p) & ","
	NExt
	$nInscriptions = StringTrimRight($nInscriptions, 1)

	iniwrite($iniFile, "Autosalvage", "inscription", $nInscriptions)

	getGWHSettings()
EndFunc

func addSuffixButtonClick()
	$curSel = _GUICtrlComboBox_GetCurSel($suffixCombo)

	IF $curSel < 0 THEN RETURN

	$cSuffixes = _GUICtrlListBox_GetCount($suffixListbox)
	if $cSuffixes < 1 Then
		iniwrite($iniFile, "Autosalvage", "mod2", GuiCtrlRead($suffixCombo))
	Else
		$t = ""
		For $p = 0 TO $cSuffixes - 1
			$t = $t & _GUICtrlListBox_GetText($suffixListBox, $p) & ","
		NEXT
		iniwrite($iniFile, "Autosalvage", "mod2", $t & GuiCtrlRead($suffixCombo))
	ENDIF

	getGWHSettings()
EndFunc

func removeSuffixButtonClick()
	$cSuffix = _GUICtrlListBox_GetCurSel($suffixListBox)

	IF $cSuffix < 0 THEN Return

	$tSuffixes = _GUICtrlListBox_GetCount($suffixListBox)
	$nSuffixes = ""
	For $p = 0 TO $tSuffixes - 1
		if $p <> $cSuffix THEN $nSuffixes = $nSuffixes & _GUICtrlListBox_GetText($suffixListBox, $p) & ","
	NExt
	$nSuffixes = StringTrimRight($nSuffixes, 1)

	iniwrite($iniFile, "Autosalvage", "mod2", $nSuffixes)

	getGWHSettings()
endfunc
;==========END GUI EVENT FUNCTIONS==========