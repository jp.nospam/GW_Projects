#cs
    Author:		jp.nospam

    Script Function:
        GW Helper global variable and constant declarations.
	----------------------------------------------------------------------------
#ce

#include-once

GLOBAL CONST $iniFile = "gwhelper.ini"

GLOBAL $HEROES[] = [ _
    "Unknown1","Norgu","Goren","Tahlkora", _
    "Master Of Whispers","Acolyte Jin","Koss", _
    "Dunkoro","Acolyte Sousuke", "Melonni", _
    "Zhed Shadowhoof", "General Morghan","Magrid the Sly", _
    "Zenmai", "Olias", "Razah","M.O.X.", "Unknown2",    _
    "Jora", "Pyre Fierceshot", "Unknown3", "Livia", _
    "Hayada", "Khamu","Gwen", _
    "Xandra", "Vekk", "Ogden", _
    "Merc1","Merc2","Merc3", _
    "Merc4","Merc5","Merc6", _
    "Merc7","Merc8", "Initiate Zei Ri"]

GLOBAL $prefixList[][] = [["survivor","CC0330250005D826"], ["blessed","C8A3D20370800006B080D20330A5"] ,["adept staff head", "2004302500140828"], _
							["centaur","050C8A30E047080000030810E0430A50"], ["shaman","03CC8A3080430A50"], ["windwalker","046C8A3040430A50"], _
							["sentinel","0050C8A3000040800014F8A0F6037080"], ["zealous dagger tang","6402302501001825640230250100C820"], ["vampiric dagger tang","6602302500032825660230250100E820"], _
							["insightful staff head","380130250500D822"], ["hale staff head","3A013025001E4823"], ["defensive staff head","2201302505000821"], _
							["furious sword hilt","360130250A00B823"]]

GLOBAL $suffixList[][] = [["vitae", "25043025000A4823"], ["major vigor", "023025C202E92700"], ["minor divine favor", "650130250110E821"], _
							["minor scythe mastery","050330250129E821"], ["minor mysticism","05033025012CE821"] ,["minor fast casting","5F0130250100E821"], _
							["minor inspiring","5F0130250103E821"] , ["minor soul reaping","610130250106E821"] ,["dagger handle of defense","8302302505000821"], _
							["pommel of fortitude","BB013025001E4823"], ["core of aptitude","2F04302500140828"], ["wand memory wrapping","BF02302500142828"], _
							["shield handle of fortitude","C3023025001E4823"], ["dagger handle of fortitude","8B023025001E4823"]]

GLOBAL $inscrList[][] = [["aptitude not attitude", "AE03322500140828"], ["forget me not", "8403322500142828"], ["i have the power","B80232250500D822"], ["luck of the draw","A403322505147820"]]

GLOBAL  $GWH_LOG = IniRead($iniFile, "Settings", "gwhLog", "False")

GLOBAL	$currentChar = FALSE
GLOBAL	$skillFolder = NULL
GLOBAL  $luxRedeem = 0
GLOBAL  $nameToggle = FALSE
GLOBAL  $interruptToggle = FALSE
GLOBAL  $botLoaded = FALSE
GLOBAL  $killBot = 0
GLOBAL  $autoloot = FALSE
GLOBAL  $autolootdistance = 1250
GLOBAL  $enemyPlayerRadius = 850
GLOBAL	$enemyItemRadius = 850
GLOBAL  $iniSettings = 0
GLOBAL  $iniHeroSet = 0
Global	$iniBotList = 0
GLOBAL  $minIDVal = 30
GLOBAL	$idWhite = FALSE
GLOBAL	$initHwnd = FALSE
Global	$iniSalvageMod1 = 0
Global  $iniSalvageMod2 = 0
Global  $iniSalvagables = FALSE
GLOBAL  $iniSalvageInscr = 0