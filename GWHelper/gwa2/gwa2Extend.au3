#cs
    Author:         jp.nospam

    Script Function:
        GWA2 extended and/or modified functions.
	----------------------------------------------------------------------------
#ce

#include-once
#include "gwa2.au3"

#cs
    Function:       ext_findBoss

    Parameters:
        $modID      Model ID of the boss. If no ID is specified, it will
					just return the first boss it finds.

    Return:
		Integer		Boss agent ID.

        -1			If no boss was found.
#ce
FUNC ext_findBoss($modID = -1)
	FOR $i = 1 TO GetMaxAgents()
		IF GetIsBoss(GetAgentById($i)) AND GetIsLiving(GetAgentById($i)) THEN
			IF $modID == -1 THEN RETURN $i
			IF $modID == dllstructgetdata(getagentbyid($i), "PlayerNumber") THEN RETURN $i
		ENDIF
	NEXT
	RETURN -1
ENDFUNC


#cs
    Function:       ext_GetIsCasting

    Parameters:
        $modID      Model ID of the boss. If no ID is specified, it will
					just return the first boss it finds.

    Return:
		True		Agent is using a skill.

		False		Agent is not using a skill.
#ce
Func ext_GetIsCasting($aAgent)
	If IsDllStruct($aAgent) = 0 Then $aAgent = GetAgentByID($aAgent)
	$aSkill = DllStructGetData($aAgent, 'Skill')
	IF $aSkill <> 0 Then

		; Brawling uppercut fix
		IF $aSkill == 2340 then return FALSE

		RETURN TRUE
	ELSE
		Return FALSE
	ENDIF
	Return DllStructGetData($aAgent, 'Skill') <> 0
EndFunc


#cs
    Function:       ext_identifyBag

    Parameters:
        $aBag		Bag id to start identifying

		$aWhites	TRUE/FALSE. Whether or not to identify white items.

		$vLimit		The minimum vendor sell price to identify whites.

		$aGolds		TRUE/FALSE. Whether or not to identify gold items.

    Return:
		Integer		Boss agent ID.

        -1			If no boss was found.
#ce
Func ext_IdentifyBag($aBag, $aWhites = false, $vLimit = 0, $aGolds = True)
	Local $lItem
	If Not IsDllStruct($aBag) Then $aBag = GetBag($aBag)
	For $i = 1 To DllStructGetData($aBag, 'Slots')
		$lItem = GetItemBySlot($aBag, $i)
		If DllStructGetData($lItem, 'ID') == 0 Then ContinueLoop
		If GetRarity($lItem) == 2621 And $aWhites == False Then ContinueLoop
		If GetRarity($lItem) == 2624 And $aGolds == False Then ContinueLoop
        if $vLimit > 0 Then
            $iValue = DllStructGetData($lItem, 'value')
            if $iValue < $vLimit and GetRarity($lItem) == 2621 then ContinueLoop
        endif
		IdentifyItem($lItem)
		Sleep(GetPing())
	Next
EndFunc

#cs
    Function:       	ext_getNearestItemToPlayer

    Parameters:
        $aCanPickup		TRUE/FALSE. Whether or not to only find items that
						you can pick up.

    Return:
		AGENT			If item was found to pickup.

		-1				If no items are available for pick up.
#ce
FUNC ext_GetNearestItemToPlayer($aCanPickUp = True)
	LOCAL $lNearestAgent = -1, $lNearestDistance = 100000000
	LOCAL $lDistance
	Local $lAgentArray = GetAgentArray(0x400)
	Local $aAgent = GetAgentByID(-2)
	Local $lID = DllStructGetData($aAgent, 'ID')

	For $i = 1 To $lAgentArray[0]

		If $aCanPickUp And GetCanPickUp($lAgentArray[$i]) == FALSE Then ContinueLoop
		$lDistance = GetDistance($lAgentArray[$i])
		If $lDistance < $lNearestDistance Then
			If DllStructGetData($lAgentArray[$i], 'ID') == $lID Then ContinueLoop
			IF dllstructgetdata(GetItemByAgentID(DllStructGetData($lAgentArray[$i], 'ID')), "type") == 6 THEN CONTINUELOOP
			$lNearestAgent = $lAgentArray[$i]
			$lNearestDistance = $lDistance
		EndIf
	Next

	SetExtended(Sqrt($lNearestDistance))
	Return $lNearestAgent
ENDFUNC

#cs
    Function:       	ext_MoveTo

    Parameters:
        $aX				X location to reach.

		$aY				Y location to reach.

		$aRandom		Randomizer.

    Return:
		None
#ce
Func ext_MoveTo($aX, $aY, $aRandom = 50)
	Local $lBlocked = 0
	Local $lMe
	Local $lDestX = $aX + Random(-$aRandom, $aRandom)
	Local $lDestY = $aY + Random(-$aRandom, $aRandom)

	Move($lDestX, $lDestY, 0)

	Do
		Sleep(100)
		IF NOT GetAgentExists(-2) Then ExitLoop

		$lMe = GetAgentByID(-2)
		If DllStructGetData($lMe, 'HP') <= 0 Then ExitLoop

		If DllStructGetData($lMe, 'MoveX') == 0 And DllStructGetData($lMe, 'MoveY') == 0 Then
			$lBlocked += 1
			$lDestX = $aX + Random(-$aRandom, $aRandom)
			$lDestY = $aY + Random(-$aRandom, $aRandom)
			Move($lDestX, $lDestY, 0)
		EndIf
	Until ComputeDistance(DllStructGetData($lMe, 'X'), DllStructGetData($lMe, 'Y'), $lDestX, $lDestY) < 25 Or $lBlocked > 14
EndFunc

#cs
    Function:       	ext_MoveTilRange

    Parameters:
        $aX				X location to reach.

		$aY				Y location to reach.

		$aRandom		Randomizer.

    Return:
		0				Made it to destination.

		1				Within range of an enemy.

		2				Died on the way to destination.

	Description:		Moves until within range of an enemy.
#ce
Func ext_MoveTilRange($aX, $aY, $aggroRange = 1250, $aRandom = 50)
	IF GetDistance(GetNearestEnemyToAgent()) <= $aggroRange Then RETURN 1

	Local $lBlocked = 0
	Local $lMe
	Local $lDestX = $aX + Random(-$aRandom, $aRandom)
	Local $lDestY = $aY + Random(-$aRandom, $aRandom)

	IF NOT GetAgentExists(-2) Then RETURN -1
	Move($lDestX, $lDestY, 0)

	Do
		Sleep(100)
		IF NOT GetAgentExists(-2) Then RETURN -1
		IF GetDistance(GetNearestEnemyToAgent()) <= $aggroRange Then RETURN 1

		$lMe = GetAgentByID(-2)

		IF DllStructGetData($lMe, 'HP') <= 0 Then RETURN 2

		If DllStructGetData($lMe, 'MoveX') == 0 And DllStructGetData($lMe, 'MoveY') == 0 Then
			$lBlocked += 1
			$lDestX = $aX + Random(-$aRandom, $aRandom)
			$lDestY = $aY + Random(-$aRandom, $aRandom)
			Move($lDestX, $lDestY, 0)
		EndIf

	Until ComputeDistance(DllStructGetData($lMe, 'X'), DllStructGetData($lMe, 'Y'), $lDestX, $lDestY) < 25 Or $lBlocked > 14

	Return 0
EndFunc

#cs
    Function:       	ext_getBagSpace

    Parameters:
		None

    Return:
        Int				Amount of bag slots free.

    Description:   		Returns then amount of bag space.

	Credits:		@gamerevision.com: anon777 (creator), mastor (update), kjohn9815 (fix)
#ce
FUNC ext_getBagSpace()
	$temp = 0

	For $b = 1 To 4
		$bag = GetBag($b)
		$temp += (DllStructGetData($bag, 'slots') - DllStructGetData($bag, 'ItemsCount'))
	next

	Return $temp
ENDFUNC