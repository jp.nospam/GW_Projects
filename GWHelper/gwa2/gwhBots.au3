#cs
    Author:         jp.nospam

    Script Function:
        GW Helper main bot functions.
	----------------------------------------------------------------------------
#ce

#include-once
#include "gwhGlobals.au3"
#include "Gwa2.au3"
#include "gwa2Extend.au3"

#cs
    Function:       	botLooter

    Parameters:
        $moveOOR		Move to out of range loot

		$ALD		   	Max radius it will search for dropped items.

        $EIR		    Radius to search for enemies around the item.
                        Will NOT loot if an enemy is within radius to item.

        $EPR			Radius to search for enemies around the player.
                        Will NOT loot if an enemy is within radius to the player.


    Return:
        None

    Description:    Wrapping function for autolooting on bots.
#ce
FUNC botLooter($moveOOR = FALSE, $ALD = -1, $EIR = -1, $EPR = -1)
	IF $ALD < 0 THEN $ALD = $autolootdistance
	if $EIR < 0 THEN $EIR = $enemyItemRadius
	if $EPR < 0 THEN $EPR = $enemyPlayerRadius

	$lootStatus = lootItems($ALD, $EIR, $EPR)
	WHILE $lootStatus <> 1
		if $lootStatus == 2 AND $moveOOR == FALSE theN Return
		if $lootstatus == 2 AND $moveOOR == TRUE Then
			$farItem = ext_GetNearestItemToPlayer()
			if $farItem == -1 then return
			Move(DLLStructGetData($farItem, "X"), DllStructGetData($farItem, "Y"))
		ENDIF
		$lootStatus = lootItems($ALD, $EIR, $EPR)
		sleep(100)
	WEND
ENDFUNC

#cs
    Function:       	poeBot

    Parameters:
		None

    Return:
        None

    Description:    Kilroy Stonekin punchout extravaganza bot.

	Credits:		@gamerevision.com: anon777 (creator), mastor (update), kjohn9815 (fix)
#ce
func poeBot()

	WHILE 1
		IF GetMapID() <> 644 THEN TravelTo(644)

		logGWH("Cleaning bags.")
		IF ext_GetBagSpace() < 20 Then
			ext_MoveTo(17969, -7345)
			GoToNPC(GetNearestNPCToCoords(17739, -7664))
			if idAll($idWhite, $minIDVal) == 0 Then
				BuyItem(5, 1, 100)
				idAll($idWhite, $minIDVal)
			ENDIF
			salvageAll($iniSalvagables)
			sellAll()
		ENDIF

		IF ext_GetBagSpace() < 20 Then
			logGWH("Unable to clean bags."&@CRLF&"Exiting.")
			exit
		ENDIF
		logGWH("Bags cleaned.")

		logGWH("Grabbing Quest.")
		GoToNPC(GetNearestNPCToCoords(17320, -4900))
		AcceptQuest(856)
		sleep(1000)
		Dialog(0x00000085)
		logGWH("Waiting for lair to load.")
		waitForMap()

		logGWH("Starting run.")
		ext_MoveTo(-16143, -13997)

		IF poeFightByCoords(-14573, -15620) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-10882, -16462) > 0 THEN CONTINUELOOP

		ext_MoveTo(-7948, -14999)
		if poeFightByCoords(-7460, -16530) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-3383, -15885) > 0 THEN CONTINUELOOP

		if poeFightByCoords(-1047, -14037) > 0 THEN CONTINUELOOP

		if poeFightByCoords(1180, -14395) > 0 THEN CONTINUELOOP

		if poeFightByCoords(3624, -16685) > 0 THEN CONTINUELOOP


		if poeFightByCoords(6987, -14660) > 0 THEN CONTINUELOOP
		if poeFightByCoords(7865, -16076) > 0 THEN CONTINUELOOP

		ext_MoveTo(9707, -16178)
		if poeFightByCoords(12445, -16063) > 0 THEN CONTINUELOOP

		logGWH("Opening and looting chest.")
		ext_MoveTo(13121, -16045)
		$chest = GetNearestSignpostToCoords(13121, -16045)
		GoToSignpost($chest)
		Sleep(300)
		botLooter(TRUE, -1, 0, 0)

		logGWH("Leaving zone.")
		IF GetMapID() <> 644 THEN TravelTo(644)

		logGWH("Turning in quest.")
		GoToNPC(GetNearestNPCToCoords(17320, -4900))
		QuestReward(856)
		Sleep(500)

		$nRegion = -2
		Switch GetRegion()

		Case -2
			$nRegion = 0

		Case 0
			$nRegion = -2

		Case Else
			$nRegion = 0

		EndSwitch
		logGWH("Changing regions.")
		MoveMap(GetMapID(), $nRegion, 0, GetLanguage())
		waitForMap()
	WEND

ENDFUNC

#cs
    Function:       	poeFightBbyCoords

    Parameters:
		$pX				X value to move to.

		$pY				Y value to move to.

		$fRadius		Radius to search for monsters.

		$fRand			Value to add randomness to coord movement.

    Return:
        0				Successfully made it to coordinates with no enemies.

		1				Map changed.

    Description:    Runs to coordinates and clears all enemies on the way.
#ce
func poeFightByCoords($pX, $pY, $fRadius = 1250, $fRand = 50)

	$mStatus = ext_MoveTilRange($pX, $pY, $fRadius, $fRand)

	WHILE $mStatus = 1
		$tBrawl = DllStructGetData(GetNearestEnemyToAgent(), "id")

		$bStatus = brawlTargetById($tBrawl)
		if $bStatus == 2 THEN RETURN 1

		$mStatus = ext_MoveTilRange($pX, $pY, $fRadius, $fRand)
	WEND

	botLooter(TRUE, -1, 0, 0)

	return 0
ENDFUNC

#cs
    Function:       	brawlTargetById

    Parameters:
		$eID			Agent ID to target.

    Return:
        0				Target is dead.

		1				Energy Recharged.

		2				Map change

    Description:		Uses brawling skills against target.

	Credits:		@gamerevision.com: anon777 (creator), mastor (update), kjohn9815 (fix)
#ce
FUNC brawlTargetById($eID)
		local $intAdrenaline[7] = [0, 0, 0, 100, 250, 175, 0]
		LOCAL $cMap = GetMapID()
		LOCAL $target = GetAgentByID($eID)

		ChangeTarget($target)
		Attack($target)

		WHILE 1
			If GetMapID() <> $cMap THEN RETURN 2

			; Stand Up
			If GetEnergy() == 0 Then
				$Me = GetAgentByID()
				$maxEnergy = DllStructGetData($Me, 'MaxEnergy')
				Do
					$Me = GetAgentByID()
					If $maxEnergy <> GetEnergy() Then UseSkill(8,$Me)
					Sleep(100)
				Until DllStructGetData(GetSkillBar(), 'Recharge8') <> 0
				RETURN 1
			EndIf

			; Fight
			$target = GetAgentByID($eID)
			For $s = 7 To 2 Step -1
				If GetSkillBarSkillId($s) > 0 AND GetSkillbarSkillRecharge($s) == 0 AND $intAdrenaline[$s-1] <= GetSkillbarSkillAdrenaline($s) Then
					UseSkill($s,$target)
					sleep(500)
					ExitLoop
				EndIf
			Next

			IF GetIsDead(GetAgentByID($eID)) OR DllStructGetData(GetAgentByID($eID), "HP") == 0 THEN RETURN 0
		WEND
ENDFUNC

#cs
    Function:       luxFarm

    Parameters:
        None

    Return:
        None

    Description:    Farm Luxon faction points at Zos Shiveros channel.

    Credits:        @gamerevision.com: Baconaise (creator)
#ce
FUNC luxFarm()
    logGWH("Starting lux farm...")
    WHILE 1
        IF GetMapID() <> 273 THEN TravelTo(273)

        IF luxMaxFaction() THEN CONTINUELOOP

        EnterChallenge()
        waitForMap()
        CommandAll(-4010, 5155)
        ext_MoveTo(-4010, 5155,0)
        $target = GetNearestEnemyToAgent(-2)
        CallTarget($target)

        $Before = GetLuxonFaction()

        DO
			Main()
            Sleep(10)
        UNTIL GetLuxonFaction() > $Before
        logGWH("Lux farm wait complete.")

        IF luxMaxFaction() THEN CONTINUELOOP

        CommandAll(-3428, 6753)
        ext_MoveTo(-3428, 6753,0)
        CallTarget(GetNearestEnemyToAgent(-2))

        $Before = GetLuxonFaction()
        logGWH("Lux farm waiting for mob death or killbot...")
        DO
            Sleep(10)
        UNTIL GetLuxonFaction() > $Before
        logGWH("Lux farm wait complete.")
    WEND

    logGWH("Lux farm complete.")
ENDFUNC

#cs
    Function:       luxMaxFaction

    Parameters:
        None

    Return:
        None

    Description:    Decides what to do when max Luxon faction.
#ce
FUNC luxMaxFaction()
    IF GetLuxonFaction() < GetMaxLuxonFaction() THEN RETURN FALSE

    TravelTo(193)
    IF $luxRedeem == 0 THEN
        EXIT
    ENDIF

    IF GetDistance(GetNearestNPCToCoords(9065, -1085), -2) > 2000 THEN
        ext_MoveTo(7573, -1568)
    ENDIF

    GoToNPC(GetNearestNPCToCoords(9065, -1085))

    SWITCH $luxRedeem
    CASE 1
        logGWH("Acquiring jade shards...")
        DO
            Dialog(0x00800101)
            Sleep(100)
        Until GetLuxonFaction() < 5000
        logGWH("jade shard acquisition complete.")
    CASE 2
        logGWH("Acquiring scrolls of the deep...")
        DO
            Dialog(0x00800102)
            Sleep(100)
        UNTIL GetLuxonFaction() < 1000
        logGWH("scroll of the deep acquisition complete.")
    CASE 3
        logGWH("Donating Luxon points...")
        DO
            DonateFaction("Luxon")
            Sleep(100)
        UNTIL GetLuxonFaction() < 5000
        logGWH("Luxon point dontation complete.")
    ENDSWITCH

    RETURN TRUE
ENDFUNC

FUNC necroEliteBot()
	IF GetMapID() <> 234 THEN TravelTo(234)

	$bossID = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 234 THEN TravelTo(234)

        ext_MoveTo(-6458, 6951, 0)
		waitForMap()

		CommandAll(11229, -1170)
		ext_MoveTo(13377, -1498, 0)
		$bossID = ext_findBoss()

		$target = GetAgentById($bossID)
		Move(dllstructgetdata($target, "X"), dllstructgetdata($target, "Y"))

		DO
			Sleep(100)
		UNTIL GetDistance(GetAgentById($bossID)) < 1250

		$target = GetAgentById(-2)
		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")
		CommandAll($moveX, $moveY)

		$target = GetAgentById($bossID)
		CancelAll()
		Attack($target)

		DO
			$target = GetAgentById($bossID)
		UNTIL GetIsDead($target) = TRUE

		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")
		sleep(200)

		DO
			$lootStatus = lootItems($autolootdistance, $enemyItemRadius, $enemyPlayerRadius)
			IF $lootStatus == 2 THEN Move($moveX, $moveY, 0)
			sleep(100)
		UNTIL  $lootStatus = 1

	WEND

ENDFUNC

FUNC ritEliteAltBot()
	IF GetMapID() <> 298 THEN TravelTo(298)

	$bossID = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 298 THEN TravelTo(298)

		ext_MoveTo(-9399, -1754, 0)
        ext_MoveTo(-10856, -4263, 0)
		ext_MoveTo(-12075, -7166, 0)
		ext_MoveTo(-13804, -7779, 0)
		waitForMap()

		CommandAll(19957, 12525)
		$bossID = ext_findBoss(4076)

		$target = GetAgentById($bossID)
		Attack($target)

		DO
			Sleep(100)
		UNTIL GetDistance(GetAgentById($bossID)) < 1250
		CancelAll()

		DO
			$target = GetAgentById($bossID)
			$selfAgent = GetAgentByID(-2)
			IF  ComputeDistance(22166, 11857 , dllstructgetdata($selfAgent, "X"), dllstructgetdata($selfAgent, "Y")) < 300 THEN CONTINUELOOP (2)
		UNTIL GetIsDead($target) = TRUE

		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")
		sleep(200)

		DO
			$lootStatus = lootItems($autolootdistance, $enemyItemRadius, $enemyPlayerRadius)
			IF $lootStatus == 2 THEN Move($moveX, $moveY, 0)
			sleep(100)
		UNTIL  $lootStatus = 1
	WEND

ENDFUNC

FUNC eleEliteBot()
	IF GetMapID() <> 287 THEN TravelTo(287)

	$bossID = 0
	SwitchMode(1)

	WHILE 1
		IF GetMapID() <> 287 THEN TravelTo(287)

        Move(27608, 5687, 0)
		waitForMap()

		IF GetDistance(GetNearestEnemyToAgent(-2))< 1400 THEN
			Move(27615, 5641, 0)
			waitForMap()
			CONTINUELOOP
		ENDIF

		CommandAll(23298, 6425)
		$bossID = ext_findBoss()

		$target = GetAgentById($bossID)
		Move(dllstructgetdata($target, "X"), dllstructgetdata($target, "Y"))

		Do
			Sleep(100)
		UNTIL GetDistance(GetAgentById($bossID)) < 1250

		$target = GetAgentById(-2)
		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")
		CommandAll($moveX, $moveY)

		$target = GetAgentById($bossID)
		CallTarget($target)
		Move(dllstructgetdata($target, "X"), dllstructgetdata($target, "Y"))

		DO
			$target = GetAgentById($bossID)
			sleep(100)
		UNTIL GetIsDead($target) = TRUE

		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")
		CancelAll()

		sleep(200)

		DO
			$lootStatus = lootItems($autolootdistance, $enemyItemRadius, $enemyPlayerRadius)
			IF $lootStatus == 2 THEN Move($moveX, $moveY, 50)
			sleep(100)
		UNTIL  $lootStatus = 1
	WEND

ENDFUNC

FUNC ritEliteBot()
	IF GetMapID() <> 51 THEN TravelTo(51)

	$bossID = 0
	SwitchMode(1)

	While 1
		IF GetMapID() <> 51 THEN TravelTo(51)

		ext_MoveTo(8106, -13937, 0)
        Move(6247, -12997, 0)
		waitForMap()

		CommandAll(8343, -9806)
		ext_MoveTo(8343, -9806, 0)
		CommandAll(8346, -7107)
		ext_MoveTo(8346, -7107, 0)
		$bossID = ext_findBoss()

		DO
			Sleep(100)
		UNTIL GetDistance(GetAgentById($bossID)) < 1250

		$target = GetAgentById($bossID)
		CancelAll()
		Attack($target)

		$selfAgent = GetAgentByID(-2)
		$curX = dllstructgetdata($selfAgent, "X")
		$curY = dllstructgetdata($selfAgent, "Y")

		DO
			$selfAgent = GetAgentByID(-2)
			$target = GetAgentById($bossID)
			IF  ComputeDistance($curX, $curY, dllstructgetdata($selfAgent, "X"), dllstructgetdata($selfAgent, "Y")) > 2500 THEN CONTINUELOOP (2)
		UNTIL GetIsDead($target) = TRUE

		$moveX = dllstructgetdata($target, "X")
		$moveY = dllstructgetdata($target, "Y")

		sleep(200)

		DO
			$lootStatus = lootItems(3000, 0, 0)
			sleep(100)
		UNTIL  $lootStatus = 1
	WEND
ENDFUNC
