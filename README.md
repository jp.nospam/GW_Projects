Random projects designed to make things easier for Guild Wars & Guild Wars 2 written in AutoIt

GW2Helper - Essentially a collection of macros and a login "saving" system with encrypted file protection

GWHelper - A fully scalable bot using gwAPI

GW2DJ - A keyboard macroing function that converts gw2mb.com scripts and also custom scripts to play instruments in Guild Wars 2